<?php

return [

    /**
     * System Constants
     */

    'non_working_days' => 20,
    'non_working_income' => 1,
    'topup_amount' => 10,
    'retopup_amount' => 15,
    'direct_income' => 2,
    'direct_retopup_income' => 3,
    'first_level_income' => 4,
    'second_level_income' => 18,
    'third_level_income' => 60,
    'min_direct' => 3,
    'pool_entry' => 100,
    'pool_amount' => 50,
    'withdrawal_min' => 10,
    'withdrawal_max' => 50,
    'withdrawal_time_min' => "08:00:00",
    'withdrawal_time_max' => "09:00:00",
    'admin_charges' => 0.1,
    'admin_bitcoinaddr' => '15G7QaX79q8vioKDGHong6Ru8NFvtXucsH',
    'direct_active_for_royal' => 10,
    'royalty_percent' => 0.01,
    'min_global_line' => 50,
    'global_income' => 20,
    'per_id_global_value' => 0.4,

];
