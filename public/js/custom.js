var base_url = window.location.origin+'/';

$('#countries').change(function(){
    var countryId = $(this).val();
    var formData = {};
    formData['countryId'] = countryId;
    var cityHtml = $('#cities');
    cityHtml.empty();
    $.get(base_url+'get-cities',formData,function(response){
        var optionHtml = '<option value="">--Select--</option>';
        $.each(response,function(key,value){
            optionHtml += '<option value="'+key+'">'+value+'</option>'
        });
        cityHtml.append(optionHtml);
        $('#cities').selectpicker('refresh');
    },'json');

});

$('#sponsorId').change(function(){
    var sponsorId = $(this).val();
    var formData = {};
    formData['sponsorId'] = sponsorId;
    $.get(base_url+'get-sponsor-details',formData,function(response){
        $('#sponsorName').val(response.name);
    });
})
