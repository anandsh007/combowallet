<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';

    protected $guarded = [];

    public function city(){
        return $this->hasMany('App\City','city_id');
    }
}
