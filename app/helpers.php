<?php

use App\GlobalPool;
use App\User;
use App\UserDetail;
use App\UserIncome;
use App\UserNetwork;
use App\UserPooled;
use Carbon\Carbon;

function getTotalTeam($username)
{
    $teamDetails = User::with('userDetails')
        ->where('sponsor_id', '=', $username)
        ->orderBy('created_at','DESC')
        ->get();
    $totalTeam = $teamDetails;
    if(count($teamDetails))
    {
        $level = 1;
        foreach ($teamDetails as $teamDetail)
        {
            $teamDetail->level = $level ;
        }
        getTeamRecursive($teamDetails,$totalTeam);

    }
    return $totalTeam;
}

function getTeamRecursive($teamDetails,$totalTeam)
{
    foreach ($teamDetails as $teamDetail)
    {
        $username = $teamDetail->user_name;
        $members = User::with('userDetails')
            ->where('sponsor_id', '=', $username)
            ->orderBy('created_at','DESC')
            ->get();
        if(count($members))
        {
            $level = $teamDetail->level;
            foreach ($members as $member)
            {
                $member->level = $level + 1 ;
                $totalTeam->push($member);
            }
            getTeamRecursive($members,$totalTeam);
        }
    }
}

function getDateTime($dateTime)
{
   $seconds = time() - strtotime($dateTime);
   $hours = $seconds / (60*60);
   return sprintf('%0.2f', $hours);
}

function nonWorkingGrowthIncome(){
    $users = User::active()->get();
    foreach($users as $user){
        $nonWorkingIncomeCount = UserIncome::where('user_id',$user->id)->where('transaction_type',UserIncome::CREDIT)->where('income_type',UserIncome::NON_WORKING)->count();
        if($nonWorkingIncomeCount < config('global.non_working_days')){
            $lastNonWorkingIncomeCreditTime = UserIncome::where('user_id',$user->id)->where('transaction_type',UserIncome::CREDIT)->where('income_type',UserIncome::NON_WORKING)->orderBy('id','DESC')->pluck('created_at')->first();
            $currentDateTime = Carbon::now();
            $currentDate = $currentDateTime->format('Y-m-d');
            $ifNonWorkingIncomeCredited = UserIncome::where('user_id',$user->id)->where('transaction_type',UserIncome::CREDIT)->where('income_type',UserIncome::NON_WORKING)->whereDate('created_at',$currentDate)->exists();
            if(!$ifNonWorkingIncomeCredited){
                if(isset($lastNonWorkingIncomeCreditTime)){
                    $hours = getDateTime($lastNonWorkingIncomeCreditTime);
                    if($hours >= 24){
                        UserIncome::create([
                            'user_id' => $user->id,
                            'amount' => config('global.non_working_income'),
                            'transaction_type' => UserIncome::CREDIT,
                            'income_type'=> UserIncome::NON_WORKING,
                        ]);
                    }
                }else{
                    $joiningDateTime = $user->activation_date;
                    $hours = getDateTime($joiningDateTime);
                    if($hours >= 24){
                        UserIncome::create([
                            'user_id' => $user->id,
                            'amount' => config('global.non_working_income'),
                            'transaction_type' => UserIncome::CREDIT,
                            'income_type'=> UserIncome::NON_WORKING,
                        ]);
                    }
                }
            }
        }
    }
}

function levelIncome(){
    $users = User::active()->get();
    foreach($users as $user){
        $activeDirectCount = getDirectActive($user->user_name);
        if($activeDirectCount >= config('global.min_direct')){
            $isEligible = firstLevelIncome($user);
            if($isEligible){
                $data = secondLevelIncome($user);
                if($data['isEligileForIncome']){
                    thirdLevelIncome($user,$data['idsForThirdLevel']);
                };
            }
        }
    }
}

function getDirectActive($username){
    $count = User::active()->where('sponsor_id',$username)->count();
    return $count;
}

function firstLevelIncome($user){
    $userNetwork = $user->network;
    $isLevelIncome = UserIncome::where('user_id',$user->id)->where('transaction_type',UserIncome::CREDIT)->where('income_type',UserIncome::LEVEL_INCOME)->where('level',1)->exists();
    $isEligible = isUserEligibleForLevelIncome($userNetwork);
    if(!$isLevelIncome && $isEligible){
        UserIncome::create([
            'user_id' => $user->id,
            'amount' => config('global.first_level_income'),
            'transaction_type' => UserIncome::CREDIT,
            'income_type' => UserIncome::LEVEL_INCOME,
            'level' => 1
        ]);
    }
    $isEligibleForSecondLevelIncome = getDirectActive($user->user_name) >= 4?true:false;
    return $isEligibleForSecondLevelIncome;
}

function secondLevelIncome($user){
    $userNetwork = $user->network;
    $isLevelIncome = UserIncome::where('user_id',$user->id)->where('transaction_type',UserIncome::CREDIT)->where('income_type',UserIncome::LEVEL_INCOME)->where('level',2)->exists();
    $ids = [$userNetwork->left_user,$userNetwork->middle_user,$userNetwork->right_user];
    $userNetworks =  UserNetwork::whereIn('user_id',$ids)->get();
    $isEligileForIncome = true;
    $idsForThirdLevel = [];
    foreach($userNetworks as $userNetwork){
        if($userNetwork->left_user != null){
            if(!isUserActive($userNetwork->left_user)){
                $isEligileForIncome = false;
            }
            array_push($idsForThirdLevel,$userNetwork->left_user);
        }else{
            $isEligileForIncome = false;
        }
        if($userNetwork->middle_user != null){
            if(!isUserActive($userNetwork->middle_user)){
                $isEligileForIncome = false;
            }
            array_push($idsForThirdLevel,$userNetwork->middle_user);
        }else{
            $isEligileForIncome = false;
        }
        if($userNetwork->right_user != null){
            if(!isUserActive($userNetwork->right_user)){
                $isEligileForIncome = false;
            }
            array_push($idsForThirdLevel,$userNetwork->right_user);
        }else{
            $isEligileForIncome = false;
        }
    }
    if($isEligileForIncome && !$isLevelIncome){
        UserIncome::create([
            'user_id' => $user->id,
            'amount' => config('global.second_level_income'),
            'transaction_type' => UserIncome::CREDIT,
            'income_type' => UserIncome::LEVEL_INCOME,
            'level' => 2
        ]);
    }
    $data = [
        'isEligileForIncome' => getDirectActive($user->user_name) >= 5?true:false,
        'idsForThirdLevel' => $idsForThirdLevel
    ];
    return $data;
}

function thirdLevelIncome($user,$ids){
    $isLevelIncome = UserIncome::where('user_id',$user->id)->where('transaction_type',UserIncome::CREDIT)->where('income_type',UserIncome::LEVEL_INCOME)->where('level',3)->exists();
    if(!$isLevelIncome){
        $userNetworks =  UserNetwork::whereIn('user_id',$ids)->get();
        $isEligileForIncome = true;
        foreach($userNetworks as $userNetwork){
            if($userNetwork->left_user != null){
                if(!isUserActive($userNetwork->left_user)){
                    $isEligileForIncome = false;
                }
            }else{
                $isEligileForIncome = false;
            }
            if($userNetwork->middle_user != null){
                if(!isUserActive($userNetwork->middle_user)){
                    $isEligileForIncome = false;
                }
            }else{
                $isEligileForIncome = false;
            }
            if($userNetwork->right_user != null){
                if(!isUserActive($userNetwork->right_user)){
                    $isEligileForIncome = false;
                }
            }else{
                $isEligileForIncome = false;
            }
        }
        if($isEligileForIncome){
            UserIncome::create([
                'user_id' => $user->id,
                'amount' => config('global.third_level_income'),
                'transaction_type' => UserIncome::CREDIT,
                'income_type' => UserIncome::LEVEL_INCOME,
                'level' => 3
            ]);
            $user->update([
                'status' => User::INACTIVE
            ]);
        }
    }
}

function isUserActive($id){
    $user = User::where('id',$id)->first();
    if($user->status == User::ACTIVE){
        return true;
    }else{
        return false;
    }
}

function isUserEligibleForLevelIncome($userNetwork){
    if($userNetwork->left_user != null && isUserActive($userNetwork->left_user) && $userNetwork->middle_user != null && isUserActive($userNetwork->middle_user) && $userNetwork->right_user != null && isUserActive($userNetwork->right_user)){
        return true;
    }else{
        return false;
    }
}

function setSpillOverNetwork($id,$newUserId){
    $isSet = false;
    $treeIds = [];
    $networks = collect(new UserNetwork);
    foreach ($id as $item) {
        $userNetwork = UserNetwork::where('user_id',$item)->first();
        $networks->push($userNetwork);
    }
    foreach($networks as $userNetwork){
        if($userNetwork->left_user == null){
            $userNetwork->update([
                'left_user' => $newUserId
            ]);
            $isSet = true;
            break;
        }elseif($userNetwork->middle_user == null){
            $userNetwork->update([
                'middle_user' => $newUserId
            ]);
            $isSet = true;
            break;
        }elseif($userNetwork->right_user == null){
            $userNetwork->update([
                'right_user' => $newUserId
            ]);
            $isSet = true;
            break;
        }else{
            array_push($treeIds,$userNetwork->left_user);
            array_push($treeIds,$userNetwork->middle_user);
            array_push($treeIds,$userNetwork->right_user);
        }
    }
    if(!$isSet){
        setSpillOverNetwork($treeIds,$newUserId);
    }
}

function networkTree($id){
    $user = User::with('network')->where('id',$id)->first();
    $data[] = [
        'id' => $user->id,
        'Name' => $user->name,
        'Username' => $user->user_name,
        'SponsorId' => $user->sponsor_id,
        'Country' => $user->userDetails->country->name,
        'img' => $user->status == User::ACTIVE?url('/')."/images/avatar-green.png":url('/')."/images/avatar-red.png"
    ];
    $data = recurTree($user->id,$data);
    return $data;
}

function recurTree($id,$data){
    $userNetwork = UserNetwork::where('user_id',$id)->first();
    if($userNetwork){
        if($userNetwork->left_user != null){
            $leftUser = getUserDetails($userNetwork->left_user);
            $newMember = [
                'id' => $leftUser->id,
                'pid' => $userNetwork->user_id,
                'Name' => $leftUser->name,
                'Username' => $leftUser->user_name,
                'SponsorId' => $leftUser->sponsor_id,
                'Country' => $leftUser->userDetails->country->name,
                'img' => $leftUser->status == User::ACTIVE?url('/')."/images/avatar-green.png":url('/')."/images/avatar-red.png"
            ];
            array_push($data,$newMember);
            $data = recurTree($leftUser->id,$data);
        }
        if($userNetwork->middle_user != null){
            $middleUser = getUserDetails($userNetwork->middle_user);
            $newMember = [
                'id' => $middleUser->id,
                'pid' => $userNetwork->user_id,
                'Name' => $middleUser->name,
                'Username' => $middleUser->user_name,
                'SponsorId' => $middleUser->sponsor_id,
                'Country' => $middleUser->userDetails->country->name,
                'img' => $middleUser->status == User::ACTIVE?url('/')."/images/avatar-green.png":url('/')."/images/avatar-red.png"
            ];
            array_push($data,$newMember);
            $data = recurTree($middleUser->id,$data);
        }
        if($userNetwork->right_user != null){
            $rightUser = getUserDetails($userNetwork->right_user);
            $newMember = [
                'id' => $rightUser->id,
                'pid' => $userNetwork->user_id,
                'Name' => $rightUser->name,
                'Username' => $rightUser->user_name,
                'SponsorId' => $rightUser->sponsor_id,
                'Country' => $rightUser->userDetails->country->name,
                'img' => $rightUser->status == User::ACTIVE?url('/')."/images/avatar-green.png":url('/')."/images/avatar-red.png"
            ];
            array_push($data,$newMember);
            $data = recurTree($rightUser->id,$data);
        }
    }
    return $data;
}

function getUserId($username){
    $userId = User::where('user_name',$username)->pluck('id')->first();
    return $userId;
}

function getUserDetails($id){
    $user = User::with('network')->where('id',$id)->first();
    return $user;
}

function getSponsorDetails($username){
    $user = User::where('user_name',$username)->first();
    return $user;
}

// function userRetopup(){
//     $users = User::active()->get();
//     foreach($users as $user){
//         $isLevelIncome = UserIncome::where('user_id',$user->id)->where('transaction_type',UserIncome::CREDIT)->where('income_type',UserIncome::LEVEL_INCOME)->where('level',3)->where('created_at','>',$user->activation_date)->exists();
//         if($isLevelIncome){
//             $user->update([
//                 'status' => User::INACTIVE
//             ]);
//         }
//     }
// }

function pooledIncome(){
    $pooledUser = UserPooled::where('status',UserPooled::PENDING)->get();
    foreach ($pooledUser as $user) {
        $count = UserPooled::where('id','>',$user->id)->count();
        if($count > config('global.pool_entry')){
            UserIncome::create([
                'user_id' => $user->user_id,
                'amount' => config('global.pool_amount'),
                'transaction_type' => UserIncome::CREDIT,
                'income_type' => UserIncome::POOL_INCOME,
            ]);
            $user->update([
                'status' => UserPooled::PAID
            ]);
        }
    }
}

function totalDownline($id){
    $user = User::with('network')->where('id',$id)->first();
    $level = 0;
    if($user->status == User::REGISTERED)
        $status = 'REGISTERED';
    elseif($user->status == User::ACTIVE)
        $status = 'ACTIVE';
    elseif($user->status == User::INACTIVE)
        $status = 'INACTIVE';
    $sponsorDetails = getSponsorDetails($user->sponsor_id);
    $data[] = [
        'id' => $user->id,
        'sponsor_id' => $user->sponsor_id,
        'sponsor_name' => $sponsorDetails->name,
        'name' => $user->name,
        'user_name' => $user->user_name,
        'email' => $user->email,
        'country' => $user->userDetails->country->name,
        'city' => $user->userDetails->city->name,
        'number' => $user->userDetails->number,
        'joining_date' => $user->created_at->format('d, M Y'),
        'status' => $status,
        'level' => $level
    ];
    $data = recurDowline($user->id,$data,$level);
    return $data;
}

function recurDowline($id,$data,$level){
    $level = $level + 1;
    $userNetwork = UserNetwork::where('user_id',$id)->first();
    if($userNetwork){
        if($userNetwork->left_user != null){
            $leftUser = getUserDetails($userNetwork->left_user);
            if($leftUser->status == User::REGISTERED)
                $status = 'REGISTERED';
            elseif($leftUser->status == User::ACTIVE)
                $status = 'ACTIVE';
            elseif($leftUser->status == User::INACTIVE)
                $status = 'INACTIVE';
            $sponsorDetails = getSponsorDetails($leftUser->sponsor_id);
            $newMember = [
                'id' => $leftUser->id,
                'pid' => $userNetwork->user_id,
                'sponsor_id' => $leftUser->sponsor_id,
                'sponsor_name' => $sponsorDetails->name,
                'name' => $leftUser->name,
                'user_name' => $leftUser->user_name,
                'email' => $leftUser->email,
                'country' => $leftUser->userDetails->country->name,
                'city' => $leftUser->userDetails->city->name,
                'number' => $leftUser->userDetails->number,
                'joining_date' => $leftUser->created_at->format('d, M Y'),
                'level' => $level,
                'status' => $status,
            ];
            array_push($data,$newMember);
            $data = recurDowline($leftUser->id,$data,$level);
        }
        if($userNetwork->middle_user != null){
            $middleUser = getUserDetails($userNetwork->middle_user);
            if($middleUser->status == User::REGISTERED)
                $status = 'REGISTERED';
            elseif($middleUser->status == User::ACTIVE)
                $status = 'ACTIVE';
            elseif($middleUser->status == User::INACTIVE)
                $status = 'INACTIVE';
            $sponsorDetails = getSponsorDetails($middleUser->sponsor_id);
            $newMember = [
                'id' => $middleUser->id,
                'pid' => $middleUser->user_id,
                'sponsor_id' => $middleUser->sponsor_id,
                'sponsor_name' => $sponsorDetails->name,
                'name' => $middleUser->name,
                'user_name' => $middleUser->user_name,
                'email' => $middleUser->email,
                'country' => $middleUser->userDetails->country->name,
                'city' => $middleUser->userDetails->city->name,
                'number' => $middleUser->userDetails->number,
                'joining_date' => $middleUser->created_at->format('d, M Y'),
                'level' => $level,
                'status' => $status,
            ];
            array_push($data,$newMember);
            $data = recurDowline($middleUser->id,$data,$level);
        }
        if($userNetwork->right_user != null){
            $rightUser = getUserDetails($userNetwork->right_user);
            if($rightUser->status == User::REGISTERED)
                $status = 'REGISTERED';
            elseif($rightUser->status == User::ACTIVE)
                $status = 'ACTIVE';
            elseif($rightUser->status == User::INACTIVE)
                $status = 'INACTIVE';
            $sponsorDetails = getSponsorDetails($rightUser->sponsor_id);
            $newMember = [
                'id' => $rightUser->id,
                'pid' => $rightUser->user_id,
                'sponsor_id' => $rightUser->sponsor_id,
                'sponsor_name' => $sponsorDetails->name,
                'name' => $rightUser->name,
                'user_name' => $rightUser->user_name,
                'email' => $rightUser->email,
                'country' => $rightUser->userDetails->country->name,
                'city' => $rightUser->userDetails->city->name,
                'number' => $rightUser->userDetails->number,
                'joining_date' => $rightUser->created_at->format('d, M Y'),
                'level' => $level,
                'status' => $status
            ];
            array_push($data,$newMember);
            $data = recurDowline($rightUser->id,$data,$level);
        }
    }
    return $data;
}

function getTotalDirectTeam($id){
    $userDetail = User::find($id);
    $totalDirectTeam = User::where('sponsor_id',$userDetail->user_name)->count();
    return $totalDirectTeam;
}

function royaltyIncome(){
    $start = Carbon::now()->startOfMonth()->format('Y-m-d');
    $end = Carbon::now()->endOfMonth()->format('Y-m-d');
    $totalMonthlyTopup = UserIncome::where('transaction_type',UserIncome::DEBIT)->where('income_type',UserIncome::TOP_PUP)->where('is_retopup','1')->whereBetween('created_at', [$start, $end])->sum('amount');
    $royaltyAmount = $totalMonthlyTopup * config('global.royalty_percent');
    $users = User::get();
    $eligibleUsers = [];
    foreach($users as $user){
        $count = getDirectActive($user->user_name);
        if($count >= config('global.direct_active_for_royal')){
            array_push($eligibleUsers,$user->id);
        }
    }
    if(count($eligibleUsers)){
        $shareForEveryUser = sprintf("%.2f", ($royaltyAmount/count($eligibleUsers)));
        if($shareForEveryUser > 0){
            foreach($eligibleUsers as $item){
                UserIncome::create([
                    'user_id' => $item,
                    'amount' => $shareForEveryUser,
                    'transaction_type' => UserIncome::CREDIT,
                    'income_type' => UserIncome::ROYALTY_INCOME
                ]);
            }
        }
    }
}

function get_local_time(){

    $ip = file_get_contents("http://ipecho.net/plain");

    $url = 'http://ip-api.com/json/'.$ip;

    $tz = file_get_contents($url);

    $tz = json_decode($tz,true)['timezone'];

    return $tz;

}

function globalLineIncome(){
    $globalPool = GlobalPool::where('status',GlobalPool::PENDING)->get();
    foreach($globalPool as $member){
        $count = GlobalPool::where('id','>',$member->id)->count();
        if($count >= config('global.min_global_line')){
            UserIncome::create([
                'user_id' => $member->user_id,
                'amount' => config('global.global_income'),
                'transaction_type' => UserIncome::CREDIT,
                'income_type' => UserIncome::GLOBAL_INCOME,
            ]);
            $member->update([
                'status' => GlobalPool::PAID
            ]);
            User::where('id',$member->user_id)->update([
                'status' => User::INACTIVE
            ]);
        }
    }
}
