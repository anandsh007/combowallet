<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalPool extends Model
{
    const PENDING = 1, PAID =2;
    protected $table = 'global_line';

    protected $guarded = [];
}
