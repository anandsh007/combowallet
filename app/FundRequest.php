<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FundRequest extends Model
{
    const PENDING = 0, PAID = 1, REJECTED =2;
    protected $table = "fund_requests";

    protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
