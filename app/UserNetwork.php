<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNetwork extends Model
{
    protected $table = 'user_networks';

    protected $guarded = [];
}
