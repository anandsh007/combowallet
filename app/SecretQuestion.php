<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecretQuestion extends Model
{
    protected $table = 'security_questions';

    protected $guarded = [];
}
