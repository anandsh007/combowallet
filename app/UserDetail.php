<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $table = 'user_details';

    protected $guarded = [];

    public function country(){
        return $this->belongsTo('App\Country','country_id');
    }
    public function city(){
        return $this->belongsTo('App\City','city_id');
    }

    public function secretQuestion(){
        return $this->belongsTo('App\SecretQuestion','sec_que_id');
    }
}
