<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::User()->is_blocked == 1){
            Auth::logout();
            return redirect('/login')->with('error','Your Account has been blocked. Please contact Administrator');
        }else{
            if( Auth::check() && Auth::User()->status == User::ACTIVE){
                return $next($request);
            }else{
                return redirect()->route('account.topup');
            }
        }
    }
}
