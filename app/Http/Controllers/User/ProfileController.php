<?php

namespace App\Http\Controllers\User;
use App\User;
use App\UserDetail;
use App\UserPassword;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserIncome;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Throwable;
use JsValidator;

class ProfileController extends Controller
{
    /**
     * The authenticated user ID.
     *
     * @var int
     */
    protected $userId;

    public function __construct()
    {
        $this->middleware(function (Request $request, $next) {
            if (!\Auth::check()) {
                return redirect('/login');
            }
            $this->userId = \Auth::id(); // you can access user id here

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function viewProfile()
    {
        $id = Auth::User()->id;
        $userDetail = User::with('userDetails')->findOrFail($id);
        return view('user.profiles.viewProfile',compact('userDetail'));
    }

    public function sponsorProfile(){
        $sponsorId = Auth::User()->sponsor_id;
        $userDetail = User::with('userDetails')->where('user_name',$sponsorId)->first();
        return view('user.profiles.sponsorProfile',compact('userDetail'));
    }

    public function showChangePassForm(){
        $validator = JsValidator::make([
            'sec_que_ans' => 'required',
            'password' => 'required|string|min:6|confirmed'
        ],[],[
            'sec_que_ans' => 'Secret Question Answer',
            'password' => 'Password'
        ]);
        $userDetail = UserDetail::where('user_id',$this->userId)->first();
        return view('user.profiles.changePassword',compact('userDetail','validator'));
    }

    public function changePassword(Request $request){
        $validation = Validator::make($request->all(), [
            'sec_que_ans' => 'required',
            'password' => 'required|string|min:6|confirmed'
        ],[],[
            'sec_que_ans' => 'Secret Question Answer',
            'password' => 'Password'
        ]);
        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation->errors());
        }
        $userDetail = UserDetail::where('user_id',$this->userId)->first();
        if($request->sec_que_ans == $userDetail->sec_que_ans){
            try{
                User::whereId($this->userId)->update([
                    'password' => Hash::make($request->password)
                ]);
                $userPassword = UserPassword::where('user_id', $this->userId)->firstOrFail();
                $userPassword->update([
                    'password' => $request->password
                ]);
                return back()->with('success','Password updated successfully!');
            }catch(\ Throwable $e){
                return back()->with('error',$e->getMessage());
            }
        }
        return back()->with('error','Sercret question answer is incorrect');
    }

    public function showBtcForm(){
        $userDetail = UserDetail::where('user_id',$this->userId)->first();
        return view('user.profiles.btcAddress',compact('userDetail'));
    }

    public function updateBtcAddress(Request $request){
        $userDetail = UserDetail::where('user_id',$this->userId)->first();
        try{
            $userDetail->update([
                'btc_address' => $request->btc_address
            ]);
            return back()->with('success','BTC Address saved successfully!');
        }catch( \ Throwable $e){
            return back()->with('error',$e->getMessage());
        }
    }

    public function showNewPassForm(){
        $validator = JsValidator::make([
            'current_password' => 'required',
            'password' => 'required|string|min:6|confirmed'
        ],[],[
            'current_password' => 'Current Password',
            'password' => 'Password'
        ]);
        return view('user.profiles.newPassword',compact('validator'));
    }

    public function newPassword(Request $request){
        $validation = Validator::make($request->all(), [
            'current_password' => 'required',
            'password' => 'required|string|min:6|confirmed'
        ],[],[
            'current_password' => 'Current Password',
            'password' => 'Password'
        ]);
        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation->errors());
        }
        $user = User::where('id',$this->userId)->first();
        $oldPassword = $user->password;
        $currentPassword = $request->current_password;
        if(Hash::check($currentPassword, $oldPassword)){
            try{
                User::whereId($this->userId)->update([
                    'password' => Hash::make($request->password)
                ]);
                $userPassword = UserPassword::where('user_id', $this->userId)->firstOrFail();
                $userPassword->update([
                    'password' => $request->password
                ]);
                return back()->with('success','Password updated successfully!');
            }catch(\ Throwable $e){
                return back()->with('error',$e->getMessage());
            }
        }
        return back()->with('error','Entered Current Password is incorrect');
    }

    public function activationHistory(){
        $data = UserIncome::where('user_id',$this->userId)->where('transaction_type',UserIncome::DEBIT)->where('income_type',UserIncome::TOP_PUP)->get();
        return view('user.profiles.activationHistory',compact('data'));
    }
}
