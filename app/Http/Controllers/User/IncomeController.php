<?php

namespace App\Http\Controllers\User;
use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserIncome;
use App\UserWallet;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Validator;
use Throwable;

class IncomeController extends Controller
{
    /**
     * The authenticated user ID.
     *
     * @var int
     */
    protected $userId;

    public function __construct()
    {
        $this->middleware(function (Request $request, $next) {
            if (!\Auth::check()) {
                return redirect('/login');
            }
            $this->userId = \Auth::id(); // you can access user id here

            return $next($request);
        });
    }

    public function growthIncome(){
        $totalWalletAmount = UserIncome::getGrowthIncome($this->userId);
        $transferredAmount = UserIncome::transferredGrowthIncome($this->userId);
        $availableAmount = $totalWalletAmount - $transferredAmount;
        return view('user.income.growthIncome',compact('availableAmount','totalWalletAmount'));
    }

    public function directIncome(){
        $totalWalletAmount = UserIncome::getTotalDirectIncome($this->userId);
        $transferredAmount = UserIncome::transferredDirectIncome($this->userId);
        $availableAmount = $totalWalletAmount - $transferredAmount;
        return view('user.income.directIncome',compact('availableAmount','totalWalletAmount'));
    }

    public function levelIncome(){
        $totalWalletAmount = UserIncome::totalLevelIncome($this->userId);
        $transferredAmount = UserIncome::transferredLevelIncome($this->userId);
        $availableAmount = $totalWalletAmount - $transferredAmount;
        return view('user.income.levelIncome',compact('availableAmount','totalWalletAmount'));
    }

    public function poolIncome(){
        $totalWalletAmount = UserIncome::totalPoolIncome($this->userId);
        $transferredAmount = UserIncome::transferredPoolIncome($this->userId);
        $availableAmount = $totalWalletAmount - $transferredAmount;
        return view('user.income.poolIncome',compact('availableAmount','totalWalletAmount'));
    }

    public function royaltyIncome(){
        $totalWalletAmount = UserIncome::totalRoyaltyIncome($this->userId);
        $transferredAmount = UserIncome::transferredRoyaltyIncome($this->userId);
        $availableAmount = $totalWalletAmount - $transferredAmount;
        return view('user.income.royalty',compact('availableAmount','totalWalletAmount'));
    }

    public function globalLineIncome(){
        $totalWalletAmount = UserIncome::totalGlobalLineIncome($this->userId);
        $transferredAmount = UserIncome::transferredGlobalLineIncome($this->userId);
        $availableAmount = $totalWalletAmount - $transferredAmount;
        return view('user.income.globalLine',compact('availableAmount','totalWalletAmount'));
    }

    public function transferFund(Request $request){
        $requestData = $request->all();
        $user = User::where('id',$this->userId)->first();
        if($requestData['source'] == UserIncome::GROWTH_INCOME_WALLET){
            if($requestData['amount'] == 10 || $requestData['amount'] == 20){
                $totalWalletAmount = UserIncome::getGrowthIncome($this->userId);
                $transferredAmount = UserIncome::transferredGrowthIncome($this->userId);
                $availableAmount = $totalWalletAmount - $transferredAmount;
                $withdrawalAllowed = false;
                if($requestData['amount'] == 10 && $availableAmount >= 10 && getDirectActive($user->user_name) >= 1){
                    $withdrawalAllowed = true;
                }elseif($requestData['amount'] == 20 && $availableAmount >= 20 && getDirectActive($user->user_name) >= 2){
                    $withdrawalAllowed = true;
                }
                if($withdrawalAllowed){
                    if($availableAmount >= $requestData['amount']){
                        UserIncome::create([
                            'user_id' => $this->userId,
                            'amount' => $requestData['amount'],
                            'transaction_type' => UserIncome::DEBIT,
                            'income_type' => UserIncome::TRANSFERRED,
                            'transferred_wallet_type' => $requestData['transferred_wallet_type'],
                            'source' => UserIncome::GROWTH_INCOME_WALLET,
                        ]);
                        $this->updateUserWallet($requestData['amount'],$requestData['transferred_wallet_type']);
                        return redirect()->back()->with('success','Fund Transferred Successfully');
                    }
                    return redirect()->back()->with('error','You don\'t have sufficient balance');
                }
                return redirect()->back()->with('error','Withdrawal Not Allowed, minimum direct active ID requirement not met');
            }
            return redirect()->back()->with('error','Incorrect Amount!');
        }elseif($requestData['source'] == UserIncome::DIRECT_INCOME_WALLET){
            $totalWalletAmount = UserIncome::getTotalDirectIncome($this->userId);
            $transferredAmount = UserIncome::transferredDirectIncome($this->userId);
            $availableAmount = $totalWalletAmount - $transferredAmount;
            if($availableAmount >= $requestData['amount']){
                UserIncome::create([
                    'user_id' => $this->userId,
                    'amount' => $requestData['amount'],
                    'transaction_type' => UserIncome::DEBIT,
                    'income_type' => UserIncome::TRANSFERRED,
                    'transferred_wallet_type' => $requestData['transferred_wallet_type'],
                    'source' => UserIncome::DIRECT_INCOME_WALLET,
                ]);
                $this->updateUserWallet($requestData['amount'],$requestData['transferred_wallet_type']);
                return redirect()->back()->with('success','Fund Transferred Successfully');
            }
            return redirect()->back()->with('error','You don\'t have sufficient balance');
        }elseif($requestData['source'] == UserIncome::LEVEL_INCOME_WALLET){
            $totalWalletAmount = UserIncome::totalLevelIncome($this->userId);
            $transferredAmount = UserIncome::transferredLevelIncome($this->userId);
            $availableAmount = $totalWalletAmount - $transferredAmount;
            if($availableAmount >= $requestData['amount']){
                UserIncome::create([
                    'user_id' => $this->userId,
                    'amount' => $requestData['amount'],
                    'transaction_type' => UserIncome::DEBIT,
                    'income_type' => UserIncome::TRANSFERRED,
                    'transferred_wallet_type' => $requestData['transferred_wallet_type'],
                    'source' => UserIncome::LEVEL_INCOME_WALLET,
                ]);
                $this->updateUserWallet($requestData['amount'],$requestData['transferred_wallet_type']);
                return redirect()->back()->with('success','Fund Transferred Successfully');
            }
            return redirect()->back()->with('error','You don\'t have sufficient balance');
        }elseif($requestData['source'] == UserIncome::POOL_INCOME_WALLET){
            $totalWalletAmount = UserIncome::totalPoolIncome($this->userId);
            $transferredAmount = UserIncome::transferredPoolIncome($this->userId);
            $availableAmount = $totalWalletAmount - $transferredAmount;
            if($availableAmount >= $requestData['amount']){
                UserIncome::create([
                    'user_id' => $this->userId,
                    'amount' => $requestData['amount'],
                    'transaction_type' => UserIncome::DEBIT,
                    'income_type' => UserIncome::TRANSFERRED,
                    'transferred_wallet_type' => $requestData['transferred_wallet_type'],
                    'source' => UserIncome::POOL_INCOME_WALLET,
                ]);
                $this->updateUserWallet($requestData['amount'],$requestData['transferred_wallet_type']);
                return redirect()->back()->with('success','Fund Transferred Successfully');
            }
            return redirect()->back()->with('error','You don\'t have sufficient balance');
        }elseif($requestData['source'] == UserIncome::ROYALTY_INCOME_WALLET){
            $totalWalletAmount = UserIncome::totalRoyaltyIncome($this->userId);
            $transferredAmount = UserIncome::transferredRoyaltyIncome($this->userId);
            $availableAmount = $totalWalletAmount - $transferredAmount;
            if($availableAmount >= $requestData['amount']){
                UserIncome::create([
                    'user_id' => $this->userId,
                    'amount' => $requestData['amount'],
                    'transaction_type' => UserIncome::DEBIT,
                    'income_type' => UserIncome::TRANSFERRED,
                    'transferred_wallet_type' => $requestData['transferred_wallet_type'],
                    'source' => UserIncome::ROYALTY_INCOME_WALLET,
                ]);
                $this->updateUserWallet($requestData['amount'],$requestData['transferred_wallet_type']);
                return redirect()->back()->with('success','Fund Transferred Successfully');
            }
            return redirect()->back()->with('error','You don\'t have sufficient balance');
        }elseif($requestData['source'] == UserIncome::GLOBAL_INCOME_WALLET){
            $totalWalletAmount = UserIncome::totalGlobalLineIncome($this->userId);
            $transferredAmount = UserIncome::transferredGlobalLineIncome($this->userId);
            $availableAmount = $totalWalletAmount - $transferredAmount;
            if($availableAmount >= $requestData['amount']){
                UserIncome::create([
                    'user_id' => $this->userId,
                    'amount' => $requestData['amount'],
                    'transaction_type' => UserIncome::DEBIT,
                    'income_type' => UserIncome::TRANSFERRED,
                    'transferred_wallet_type' => $requestData['transferred_wallet_type'],
                    'source' => UserIncome::GLOBAL_INCOME_WALLET,
                ]);
                $this->updateUserWallet($requestData['amount'],$requestData['transferred_wallet_type']);
                return redirect()->back()->with('success','Fund Transferred Successfully');
            }
            return redirect()->back()->with('error','You don\'t have sufficient balance');
        }
    }

    public function updateUserWallet($amount,$walletType){
        $userWallet = UserWallet::Where('user_id',$this->userId)->first();
        if($walletType == UserIncome::MAIN_WALLET){
            if($userWallet){
                $newAmount = $userWallet->amount + $amount;
                $userWallet->update([
                    'amount' => $newAmount
                ]);
            }else{
                UserWallet::create([
                    'user_id' => $this->userId,
                    'amount' => $amount
                ]);
            }
        }else{
            if($userWallet){
                $newAmount = $userWallet->top_up_wallet + $amount;
                $userWallet->update([
                    'top_up_wallet' => $newAmount
                ]);
            }else{
                UserWallet::create([
                    'user_id' => $this->userId,
                    'top_up_wallet' => $amount
                ]);
            }
        }
    }

}
