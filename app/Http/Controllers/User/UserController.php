<?php

namespace App\Http\Controllers\User;

use App\FundRequest;
use App\GlobalPool;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use App\UserIncome;
use App\UserPooled;
use App\UserWallet;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Carbon\Traits\Timestamp;
use DB;
use Throwable;

class UserController extends Controller
{
    /**
     * The authenticated user ID.
     *
     * @var int
     */
    protected $userId;

    public function __construct()
    {
        $this->middleware(function (Request $request, $next) {
            if (!\Auth::check()) {
                return redirect('/login');
            }
            $this->userId = \Auth::id(); // you can access user id here

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $id = Auth::User()->id;
        $userDetail = User::findOrFail($id);
        $totalDirectTeam = User::where('sponsor_id',$userDetail->user_name)->count();
        $totalTeam = count(totalDownline($id)) - 1;
        $poolEntry = UserPooled::where('user_id',$id)->count();
        $globalLineIncome = 0;
        if(GlobalPool::where('user_id',$id)->where('status',GlobalPool::PENDING)->exists()){
            $dataGlobal = GlobalPool::where('user_id',$id)->where('status',GlobalPool::PENDING)->first();
            $count = GlobalPool::where('id','>',$dataGlobal->id)->count();
            $globalLineIncome = $count * config('global.per_id_global_value');
        }
        $totalIncome = UserIncome::getGrowthIncome($id) + UserIncome::getTotalDirectIncome($id) + UserIncome::totalLevelIncome($id) + UserIncome::totalPoolIncome($id) + UserIncome::totalRoyaltyIncome($id) + UserIncome::totalGlobalLineIncome($id);
        $mainWallet = UserWallet::where('user_id',$id)->pluck('amount')->first();
        $topupFund = UserWallet::where('user_id',$id)->pluck('top_up_wallet')->first();
        $totalRoyaltyIncome = UserIncome::totalRoyaltyIncome($id);
        $transferredRoyaltyIncome = UserIncome::transferredRoyaltyIncome($id);
        $availableRoyaltyAmount = $totalRoyaltyIncome - $transferredRoyaltyIncome;
        $news = News::where('type','horizontal')->first();
        return view('user.dashboard',compact('userDetail','totalDirectTeam','totalTeam','poolEntry','totalIncome','mainWallet','topupFund','availableRoyaltyAmount','globalLineIncome','news'));
    }

    public function showTopupForm(){
        $ifPreActivated = UserIncome::where('user_id',$this->userId)->where('transaction_type',UserIncome::DEBIT)->where('income_type',UserIncome::TOP_PUP)->exists();
        $userWallet = UserWallet::where('user_id',$this->userId)->first();
        return view('user.topup',compact('userWallet','ifPreActivated'));
    }

    public function topup(Request $request){
        $userWallet = UserWallet::where('user_id',$this->userId)->first();
        if($userWallet->top_up_wallet >= $request->amount){
            DB::beginTransaction();
            $dbStatus = false;
            try{
                $ifPreActivated = UserIncome::where('user_id',$this->userId)->where('transaction_type',UserIncome::DEBIT)->where('income_type',UserIncome::TOP_PUP)->exists();
                $isRetopup = $ifPreActivated? 1:0;
                $created1 = UserIncome::create([
                            'user_id' => $this->userId,
                            'amount' => $request->amount,
                            'transaction_type' => UserIncome::DEBIT,
                            'income_type' => UserIncome::TOP_PUP,
                            'source' => UserIncome::MAIN_WALLET,
                            'is_retopup' => $isRetopup
                        ]);
                $sponsor_id = User::where('user_name',Auth::User()->sponsor_id)->pluck('id')->first();
                if($ifPreActivated){
                    $directAmount = config('global.direct_retopup_income');
                }else{
                    $directAmount = config('global.direct_income');
                }
                $created2 = UserIncome::create([
                    'user_id' => $sponsor_id,
                    'amount' => $directAmount,
                    'transaction_type' => UserIncome::CREDIT,
                    'income_type' => UserIncome::DIRECT_INCOME
                ]);

                $updated1 = User::where('id',$this->userId)->update([
                                'status' => User::ACTIVE,
                                'activation_date' => Carbon::now()->format('Y-m-d H:i:s')
                            ]);
                $newAmount = $userWallet->top_up_wallet - $request->amount;
                $updated2 = $userWallet->update([
                                'top_up_wallet' => $newAmount
                            ]);
                if($created1 && $created2 && $updated1 && $updated2){
                    $dbStatus = true;
                }
                if($ifPreActivated){
                    $pooled = UserPooled::create([
                                'user_id' => $this->userId
                            ]);
                    $globalLine = GlobalPool::create([
                        'user_id' => $this->userId
                    ]);
                    if($dbStatus && $pooled && $globalLine){
                        $dbStatus = true;
                    }
                }else{
                    setSpillOverNetwork([$sponsor_id],Auth::User()->id);
                }
            }catch(\ Throwable $e){
                return redirect()->back()->with('error',$e->getMessage());
            }
            if($dbStatus){
                DB::commit();
                return redirect()->route('user.index')->with('success','Top up successfull');
            }
            else{
                DB::rollBack();
                return redirect()->back()->with('error','Something went wrong');
            }
        }else{
            return redirect()->back()->with('error','Insufficient Fund');
        }
    }

    public function fileUpload(Request $request){
        $requestData = $request->all();
        if($request->hasFile('file'))
        {
            $uploadFile = time().'_'.str_replace(" ","_",$request->file('file')->getClientOriginalName());
            $request->file('file')->move("uploads/proof-files/",$uploadFile);
            $response = [
                'status' => 'ok',
                'file' => $uploadFile
            ];
            return response($response);
        }
        $response = [
            'status' => 'error'
        ];
        return response($response);
    }

    public function fundRequest(Request $request){
        $requestData = $request->all();
        if($requestData['amount'] > 0 && filter_var($requestData['amount'], FILTER_VALIDATE_INT) && $requestData['amount']%10 == 0){
            FundRequest::create([
                'user_id' => $this->userId,
                'amount' => $requestData['amount'],
                'proof' => $requestData['file'],
                'remark' => $requestData['remark'],
                'status' => FundRequest::PENDING,
            ]);
            $response = [
                'status' => "ok"
            ];
        }else{
            $response = [
                'status' => "error",
                'message' => 'Invalid Amount!'
            ];
        }
        return response($response);
    }


}
