<?php

namespace App\Http\Controllers\User;

use App\GlobalPool;
use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserIncome;
use App\UserWallet;
use App\UserPooled;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Validator;
use Throwable;
use JsValidator;

class WalletController extends Controller
{
    /**
     * The authenticated user ID.
     *
     * @var int
     */
    protected $userId;

    public function __construct()
    {
        $this->middleware(function (Request $request, $next) {
            if (!\Auth::check()) {
                return redirect('/login');
            }
            $this->userId = \Auth::id(); // you can access user id here

            return $next($request);
        });
    }

    public function showTransferForm(){
        $walletAmount = UserWallet::where('user_id',$this->userId)->pluck('amount')->first();
        if(!$walletAmount){
            $walletAmount = 0;
        }
        return view('user.wallet.transferFund',compact('walletAmount'));
    }

    public function transferFund(Request $request){
        $userWallet = UserWallet::where('user_id',$this->userId)->first();
        if($request->amount%10 == 0){
            if($userWallet){
                if($userWallet->amount >= $request->amount){
                    DB::beginTransaction(); // <-- first line
                    $saved = true;
                    try{
                        $insertedFromUser = UserIncome::create([
                            'user_id' => $this->userId,
                            'amount' => $request->amount,
                            'income_type' => UserIncome::TRANSFERRED,
                            'transaction_type' => UserIncome::DEBIT,
                            'source' => UserIncome::MAIN_WALLET,
                            'to_user_id' => $request->userId
                        ]);
                        $insertedToUser = UserIncome::create([
                            'user_id' => $request->userId,
                            'amount' => $request->amount,
                            'income_type' => UserIncome::TRANSFERRED,
                            'transaction_type' => UserIncome::CREDIT,
                            'transferred_wallet_type' => UserIncome::TOP_UP_WALLET
                        ]);
                        $updatedAmount = $userWallet->amount - $request->amount;
                        $updated = $userWallet->update([
                                        'amount' => $updatedAmount
                                    ]);
                        $recieverWallet = UserWallet::where('user_id',$request->userId)->first();
                        if($recieverWallet){
                            $recUpdatedAmount = $recieverWallet->top_up_wallet + $request->amount;
                            $recieverWallet->update([
                                'top_up_wallet' => $recUpdatedAmount
                            ]);
                        }else{
                            UserWallet::create([
                                'user_id' => $request->userId,
                                'top_up_wallet' => $request->amount
                            ]);
                        }
                        if($insertedFromUser && $insertedToUser && $updated)
                            $saved = true;
                        else
                            $saved = false;
                    }catch(\ Throwable $e){
                        $response = [
                            'status' => 'error',
                            'message' => $e->getMessage()
                        ];
                    }
                    if($saved)
                    {
                        DB::commit(); // YES --> finalize it
                        $response = [
                            'status' => 'ok',
                            'message' => 'Fund transferred successfully',
                            'walletAmount' => $updatedAmount
                        ];
                    }
                    else
                    {
                        DB::rollBack(); // NO --> some error has occurred undo the whole thing
                        $response = [
                            'status' => 'error',
                            'message' => 'Something went wrong'
                        ];
                    }
                }else{
                    $response = [
                        'status' => 'error',
                        'message' => 'Insufficient Fund'
                    ];
                }
            }else{
                $response = [
                    'status' => 'error',
                    'message' => 'Insufficient Fund'
                ];
            }
        }else{
            $response = [
                'status' => 'error',
                'message' => 'Amount must be multiple of 10'
            ];
        }

        return response()->json($response);
    }

    public function showWithdrawForm(){
        $walletAmount = UserWallet::where('user_id',$this->userId)->pluck('amount')->first();
        if(!$walletAmount){
            $walletAmount = 0;
        }
        return view('user.wallet.withdraw',compact('walletAmount'));
    }

    public function withdrawFund(Request $request){
        $requestData = $request->all();
        $totalWalletAmount = UserWallet::where('user_id',$this->userId)->pluck('amount')->first();
        $userDetail = UserDetail::where('user_id',$this->userId)->first();
        if($userDetail->btc_address != "" && $userDetail->btc_address != null){
            if($totalWalletAmount >= $requestData['amount']){
                if($requestData['amount'] >= config('global.withdrawal_min') && $requestData['amount'] <= config('global.withdrawal_max')){
                    $currentTime = carbon::now('Asia/Bangkok')->format('H:i:s');
                    if($currentTime >= config('global.withdrawal_time_min') && $currentTime <= config('global.withdrawal_time_max')){
                        $currentDate = Carbon::now()->format('Y-m-d');
                        $withdrawal = UserIncome::where('user_id',$this->userId)->where('transaction_type',UserIncome::DEBIT)->where('income_type',UserIncome::WITHDRAW)->whereDate('created_at',$currentDate)->exists();
                        if(!$withdrawal){
                            $withdrawalAmount = $requestData['amount'] - ($requestData['amount'] * config('global.admin_charges'));
                            UserIncome::create([
                                'user_id' => $this->userId,
                                'amount' => $withdrawalAmount,
                                'transaction_type' => UserIncome::DEBIT,
                                'income_type' => UserIncome::WITHDRAW,
                                'source' => UserIncome::MAIN_WALLET,
                                'status' => UserIncome::PENDING
                            ]);
                            $this->updateUserWallet($requestData['amount']);
                            return redirect()->back()->with('success','Thank you for withdrwal it will be processed after 72 hours.');
                        }
                        return redirect()->back()->with('error','Withdrawal Day limit once per day');
                    }
                    return redirect()->back()->with('error','Withdrawal Time is  8:00 - 9:00 AM');
                }
                return redirect()->back()->with('error','Invalid Amount');
            }
            return redirect()->back()->with('error','You don\'t have sufficient balance');
        }else{
            return redirect()->back()->with('error','BTC address is not available');
        }
    }

    public function updateUserWallet($amount){
        $userWallet = UserWallet::Where('user_id',$this->userId)->first();
        if($userWallet){
            $newAmount = $userWallet->amount - $amount;
            $userWallet->update([
                'amount' => $newAmount
            ]);
        }
    }

    public function topupUserIdForm(){
        $walletAmount = UserWallet::where('user_id',$this->userId)->pluck('top_up_wallet')->first();
        if(!$walletAmount){
            $walletAmount = 0;
        }
        return view('user.wallet.topup',compact('walletAmount'));
    }

    public function topupId(Request $request){
        $userWallet = UserWallet::where('user_id',$this->userId)->first();
        $userStatus = User::where('id',$request->userId)->pluck('status')->first();
        if($userStatus != User::ACTIVE){
            if($userWallet->top_up_wallet >= $request->amount){
                DB::beginTransaction();
                $dbStatus = false;
                try{
                    $ifPreActivated = UserIncome::where('user_id',$request->userId)->where('transaction_type',UserIncome::DEBIT)->where('income_type',UserIncome::TOP_PUP)->exists();
                    $isRetopup = $ifPreActivated? 1:0;
                    $created1 = UserIncome::create([
                                'user_id' => $request->userId,
                                'amount' => $request->amount,
                                'transaction_type' => UserIncome::DEBIT,
                                'income_type' => UserIncome::TOP_PUP,
                                'source' => UserIncome::USER_WALLET,
                                'from_user_id' => $this->userId,
                                'is_retopup' => $isRetopup
                            ]);
                    $sponsor_id = User::where('id',$request->userId)->pluck('sponsor_id')->first();
                    $sponsoredUserId = User::where('user_name',$sponsor_id)->pluck('id')->first();
                    if($ifPreActivated){
                        $directAmount = config('global.direct_retopup_income');
                    }else{
                        $directAmount = config('global.direct_income');
                    }
                    $created2 = UserIncome::create([
                        'user_id' => $sponsoredUserId,
                        'amount' => $directAmount,
                        'transaction_type' => UserIncome::CREDIT,
                        'income_type' => UserIncome::DIRECT_INCOME
                    ]);
                    $updated1 = User::where('id',$request->userId)->update([
                                    'status' => User::ACTIVE,
                                    'activation_date' => Carbon::now()->format('Y-m-d H:i:s')
                                ]);
                    $newAmount = $userWallet->top_up_wallet - $request->amount;
                    $updated2 = $userWallet->update([
                                    'top_up_wallet' => $newAmount
                                ]);
                    if($created1 && $created2 && $updated1 && $updated2){
                        $dbStatus = true;
                    }
                    if($ifPreActivated){
                        $pooled = UserPooled::create([
                                    'user_id' => $request->userId
                                ]);
                        $globalLine = GlobalPool::create([
                            'user_id' => $request->userId
                        ]);
                        if($dbStatus && $pooled && $globalLine){
                            $dbStatus = true;
                        }
                    }else{
                        setSpillOverNetwork([$sponsoredUserId],$request->userId);
                    }
                }catch(\ Throwable $e){
                    $response = [
                        'status' => 'error',
                        'message' => $e->getMessage()
                    ];
                }
                if($dbStatus){
                    DB::commit();
                    $response = [
                        'status' => 'ok',
                        'message' => 'User Id Top up successfull',
                        'walletAmount' => $newAmount
                    ];
                }
                else{
                    DB::rollBack();
                    $response = [
                        'status' => 'error',
                        'message' => 'Something went wrong'
                    ];
                }
            }else{
                $response = [
                    'status' => 'error',
                    'message' => 'Insufficient Fund'
                ];
            }
        }else{
            $response = [
                'status' => 'error',
                'message' => 'User Already Activated '
            ];
        }
        return response()->json($response);
    }

    public function userFundRequest(){
        $userWallet = UserWallet::where('user_id',$this->userId)->first();
        return view('user.wallet.fundRequest',compact('userWallet'));
    }

    public function withdrawalRequest(){
        $withdrawalRequests = UserIncome::where('user_id',$this->userId)->where('transaction_type',UserIncome::DEBIT)->where('income_type',UserIncome::WITHDRAW)->where('source',UserIncome::MAIN_WALLET)->get();
        return view('user.wallet.withdrawalList',compact('withdrawalRequests'));
    }

}
