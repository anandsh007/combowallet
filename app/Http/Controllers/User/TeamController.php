<?php

namespace App\Http\Controllers\User;
use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Validator;
use Throwable;
use JsValidator;

class TeamController extends Controller
{
    /**
     * The authenticated user ID.
     *
     * @var int
     */
    protected $userId;

    public function __construct()
    {
        $this->middleware(function (Request $request, $next) {
            if (!\Auth::check()) {
                return redirect('/login');
            }
            $this->userId = \Auth::id(); // you can access user id here

            return $next($request);
        });
    }

    public function registeredTeam(){
        $userName = User::whereId($this->userId)->pluck('user_name')->first();
        $team = User::where('sponsor_id',$userName)->where('status',User::REGISTERED)->get();
        return view('user.team.registered',compact('team'));
    }

    public function directTeam(){
        $team = User::getDirectTeam($this->userId);
        return view('user.team.directTeam',compact('team'));
    }

    public function totalTeam(){
        $id = Auth::User()->id;
        $team = totalDownline($id);
        return view('user.team.totalTeam',compact('team'));

    }

    public function network(){
        $username = Auth::User()->user_name;
        $network = networkTree($this->userId);
        $jsonData = json_encode($network);
        return view('user.team.network',compact('jsonData'));
    }

    public function networkChart(){
        $id = Auth::User()->id;
        $team = totalDownline($id);
        usort($team, function($a,$b){
            if ($a == $b)
                return 0;
            return ($a['level'] < $b['level']) ? -1 : 1;
        });
        return view('user.team.networkChart',compact('team'));
    }
}
