<?php

namespace App\Http\Controllers;

use App\News;
use App\Contact;
use JsValidator;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
        'user_name' => 'required|exists:users,user_name|max:255',
        'password' => 'required|min:6',
    ];

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $news = News::where('type','vertical')->first();
        $validator = JsValidator::make($this->validationRules);
        return view('welcome',compact('validator','news'));
    }

    public function plan(){
        return view('plan');
    }

    public function contact(){
        return view('contact');
    }

    public function policy(){
        return view('terms');
    }

    public function legal(){
        return view('legal');
    }

    public function submit(Request $request){
        $requestData = $request->all();
        Contact::create($requestData);
        return redirect()->back()->with('success', 'Your Query has been submited!');
    }

    public function logout(){
        Auth::logout();
        return redirect()->back();
    }
}
