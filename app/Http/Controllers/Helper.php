<?php

namespace App\Http\Controllers;

use App\User;
use App\City;
use App\UserIncome;
use Illuminate\Http\Request;

class Helper extends Controller
{
    public function validateUserId(Request $request){
        $this->validate($request,[
            'user_id' => 'required|exists:users,user_name'
        ]);
    }

    public function getUserDetails($userId){
        $user = User::Where('user_name',$userId)->first();
        if($user){
            $ifPreActivated = UserIncome::where('user_id',$user->id)->where('transaction_type',UserIncome::DEBIT)->where('income_type',UserIncome::TOP_PUP)->exists();
            $maskedEmail = $this->obfuscate_email($user->email);
            $response = [
                'status' => 'ok',
                'userId' => $user->id,
                'name' => $user->name,
                'userName' => $user->user_name,
                'email' => $user->email,
                'country' => $user->userDetails->country->name,
                'city' => $user->userDetails->city->name,
                'number' => $user->userDetails->number,
                'btcAddress' => $user->userDetails->btc_address,
                'isPreactivated' => $ifPreActivated,
                'maskedEmail' => $maskedEmail,
                'retopUpAmount' => config('global.retopup_amount')
            ];
        }else{
            $response = [
                'status' => 'error',
                'message' => 'User doesn\'t exist'
            ];
        }
        return response()->json($response);
    }

    public function obfuscate_email($email)
    {
        $em   = explode("@",$email);
        $name = implode('@', array_slice($em, 0, count($em)-1));
        $len  = floor(strlen($name)/2);

        return substr($name,0, $len) . str_repeat('*', $len) . "@" . end($em);
    }

    /**
     * Get All Cities of Country
     *
     * @param int $countryId
     * @return array $cities
     */
    public function getCities(Request $request){
        $countryId = $request->countryId;
        $cities = City::where('country_id',$countryId)->orderBy('name','ASC')->pluck('name','id')->toArray();
        return response()->json($cities);
    }

    /**
     * Get Sponsor Details
     *
     * @param string $sponsorId
     * @return array
     */
    public function getSponsorDetails(Request $request){
        $sponsorDetails = User::where('user_name',$request->sponsorId)->first();
        if($sponsorDetails){
            $response = [
                'status' => 'ok',
                'name' => $sponsorDetails->name,
            ];
            return response()->json($response);
        }
    }
}
