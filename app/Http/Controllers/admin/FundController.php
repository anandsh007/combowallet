<?php


namespace App\Http\Controllers\admin;

use App\FundRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\UserWallet;
use App\UserIncome;
use DB;
use Illuminate\Http\Request;

class FundController extends Controller
{

    public function addFundForm()
    {
        return view('admin.fund.add');
    }

    public function addFund(Request $request)
    {
        $userWallet = UserWallet::where('user_id',$request->userId)->first();
        DB::beginTransaction(); // <-- first line
        $saved = true;
        try{
            $userIncome = UserIncome::create([
                'user_id' => $request->userId,
                'amount' => $request->amount,
                'income_type' => UserIncome::ADMIN,
                'transaction_type' => UserIncome::CREDIT,
                'transferred_wallet_type' => UserIncome::TOP_UP_WALLET
            ]);
            if($userWallet){
                $updatedAmount = $userWallet->top_up_wallet + $request->amount;
                $userWallet->update([
                    'top_up_wallet' => $updatedAmount
                ]);
            }else{
                $userWallet = UserWallet::create([
                    'user_id' => $request->userId,
                    'top_up_wallet' => $request->amount
                ]);
            }
            if(!$userIncome || !$userWallet){
                $saved = false;
            }
        }catch(\ Throwable $e){
            $response = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }
        if($saved)
        {
            DB::commit(); // YES --> finalize it
            $response = [
                'status' => 'ok',
                'message' => 'Fund transferred successfully',
                'walletAmount' => $updatedAmount
            ];
        }
        else
        {
            DB::rollBack(); // NO --> some error has occurred undo the whole thing
            $response = [
                'status' => 'error',
                'message' => 'Something went wrong'
            ];
        }
        return response()->json($response);
    }

    public function fundList()
    {
        $fundsList = UserIncome::where('transaction_type',UserIncome::CREDIT)->where('income_type',UserIncome::ADMIN)->get();
        return view('admin.fund.list',compact('fundsList'));
    }

    public function fundRequest(){
        $fundRequests = FundRequest::where('status',FundRequest::PENDING)->get();
        return view('admin.fund.request',compact('fundRequests'));
    }

    public function changeFundRequestStatus(Request $request){
        if($request->has('action')){
            $fundRequest = FundRequest::where('id',$request->requestId)->first();
            if($request->action == FundRequest::PAID){
                $userWallet = UserWallet::where('user_id',$request->userId)->first();
                $userIncome = UserIncome::create([
                    'user_id' => $request->userId,
                    'amount' => $fundRequest->amount,
                    'income_type' => UserIncome::ADMIN,
                    'transaction_type' => UserIncome::CREDIT,
                    'transferred_wallet_type' => UserIncome::TOP_UP_WALLET
                ]);
                if($userWallet){
                    $updatedAmount = $userWallet->top_up_wallet + $fundRequest->amount;
                    $userWallet->update([
                        'top_up_wallet' => $updatedAmount
                    ]);
                }else{
                    $userWallet = UserWallet::create([
                        'user_id' => $request->userId,
                        'top_up_wallet' => $fundRequest->amount
                    ]);
                }
            }
            $fundRequest->update([
                'status' => $request->action
            ]);
        }
        return redirect()->back()->with('success','Status Changed successfully');
    }
}
