<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Country;
use App\User;
use App\UserDetail;
Use App\News;
use App\Http\Controllers\Controller;
use App\UserIncome;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $totalSystemId = User::count();
        $totalNewId = User::where('status',User::REGISTERED)->count();
        $registeredLists = User::where('status',User::REGISTERED)->get();
        $totalInActiveId = User::where('status',User::INACTIVE)->count();
        $totalActiveId = User::where('status',User::ACTIVE)->count();
        $totalBlockedId = User::where('is_blocked',1)->count();
        $blockedLists = User::where('is_blocked',1)->get();
        $totalSystemFund = UserIncome::where('transaction_type',UserIncome::DEBIT)->where('income_type',UserIncome::TOP_PUP)->sum('amount');
        $totalTopupFund = UserIncome::where('transaction_type',UserIncome::DEBIT)->where('income_type',UserIncome::TOP_PUP)->where('is_retopup',0)->sum('amount');
        $totalReTopupFund = UserIncome::where('transaction_type',UserIncome::DEBIT)->where('income_type',UserIncome::TOP_PUP)->where('is_retopup',1)->sum('amount');
        $withdrawedFund = UserIncome::where('transaction_type',UserIncome::DEBIT)->where('income_type',UserIncome::WITHDRAW)->where('source',UserIncome::MAIN_WALLET)->where('status',UserIncome::PAID)->sum('amount');
        $totalBalanceFund = ($totalTopupFund + $totalReTopupFund) - $withdrawedFund;
        $totalAddedFund = UserIncome::where('transaction_type',UserIncome::CREDIT)->where('income_type',UserIncome::ADMIN)->sum('amount');;
        $start = Carbon::now()->startOfMonth()->format('Y-m-d');
        $end = Carbon::now()->endOfMonth()->format('Y-m-d');
        $totalMonthlyTopup = UserIncome::where('transaction_type',UserIncome::DEBIT)->where('income_type',UserIncome::TOP_PUP)->where('is_retopup','1')->whereBetween('created_at', [$start, $end])->sum('amount');
        $royaltyFund = $totalMonthlyTopup;
        $users = User::get();
        $eligibleUsers = [];
        $royaltyLists = collect(new User);
        foreach($users as $user){
            $count = getDirectActive($user->user_name);
            if($count >= config('global.direct_active_for_royal')){
                array_push($eligibleUsers,$user->id);
                $royaltyLists->push($user);
            }
        }
        $totalAchiever = count($eligibleUsers);
        $news = News::where('type','horizontal')->first();
        return view('admin.dashboard', compact('totalSystemId','totalActiveId','totalNewId','totalInActiveId','totalBlockedId','totalSystemFund','totalBalanceFund','totalAddedFund','totalTopupFund','totalReTopupFund','news','royaltyFund','totalAchiever','blockedLists','royaltyLists','registeredLists'));
    }

    public function withdrawalsList(Request $request){
        $whereData = [];
        if($request->has('status') && $request->get('status') != ""){
            $whereData = ['status' => $request->status];
        }
        $withdrawals = UserIncome::where('transaction_type',UserIncome::DEBIT)->where('income_type',UserIncome::WITHDRAW)->where($whereData)->get();
        return view('admin.withdrawals',compact('withdrawals'));
    }

    public function withdrawalsPaid($id){
        UserIncome::where('id',$id)->update([
            'status' => UserIncome::PAID
        ]);
        return redirect()->back()->with('success','Status changed to Paid successfully');
    }

    public function withdrawalsRejected($id){
        UserIncome::where('id',$id)->update([
            'status' => UserIncome::REJECTED
        ]);
        return redirect()->back()->with('success','Status changed to Rejected successfully');
    }

    public function addcityForm(){
        $countries = Country::orderBy('name')->get();
        return view('admin.addcity',compact('countries'));
    }

    public function addCity(Request $request){
        $country = Country::where('id',$request->country_id)->first();
        City::create([
            'country_id' => $request->country_id,
            'country' => $country->name,
            'name' => $request->city
        ]);
        return redirect()->back()->with('success','city added successfully');
    }
}
