<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\UserDetail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
class DownlineController extends Controller
{

    public function networkTree(Request $request){
        $username = User::pluck('user_name','id')->toArray();
        $jsonData = [];
        if($request->has('user_name')){
            $requestData = $request->get('user_name');
            $jsonData = networkTree($requestData);
        }
        $jsonData = json_encode($jsonData);
        return view('admin.downline.network',compact('jsonData','username'));
    }

    public function totalDownline(Request $request)
    {
        $requestData = $request->get('user_name');
        $username = User::pluck('user_name','id')->toArray();
        $teamDetails = null;
        if(!empty($requestData))
        {
            $teamDetails = totalDownline($requestData);
            // Sort the array
            usort($teamDetails, function($element1, $element2){
                return $element1['level'] - $element2['level'];
            });
            return view('admin.downline.total',compact('teamDetails','username'));
        }
        else{
            return view('admin.downline.total',compact('teamDetails','username'));
        }
    }
    public function directTeam(Request $request)
    {
        $requestData = $request->get('user_name');
        $username = User::pluck('user_name','user_name')->toArray();
        $teamDetails = null;
        if(!empty($requestData))
        {
            $teamDetails = User::with('UserDetails')
                         ->where('sponsor_id', '=', $requestData)
                         ->get();
            return view('admin.downline.direct',compact('teamDetails','username'));
        }
        else{
            return view('admin.downline.direct',compact('teamDetails','username'));
        }

    }

    public function blockedMembers(Request $request)
    {
        $teamDetails = User::with('UserDetails')
                    ->Where('status',User::BLOCKED)
                    ->orderBy('updated_at','DESC')
                    ->get();
        return view('admin.downline.blocked',compact('teamDetails'));
    }
}
