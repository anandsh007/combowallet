<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Http\Controllers\Controller;
use App\User;
use App\UserDetail;
use App\UserPassword;
use App\ExportData;
use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Excel;
use JsValidator;
use Validator;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        $users = User::orderBy('created_at','DESC')->get();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $countries = Country::select('id','name')->get();
        $cities = City::where('country_id',$user->userDetails->country_id)->get();
        return view('admin.users.edit', compact('user','countries','cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int      $id
     *
     * @return void
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'number' => 'required',
            'email' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
        ]);
        $user = User::findOrFail($id);
        $data = $request->all();
        if (!empty($data['password'])) {
            $password = $request->password;
            $userPassword = UserPassword::where('user_id', $id)->firstOrFail();
            $userPassword->update([
                'password' => $password
            ]);
            $data['password'] = bcrypt($request->password);
            $user->update([
                'name' => strtoupper($request->name),
                'email' => strtolower($request->email),
                'password' => $data['password']
            ]);
        }
        else{
            $user->update([
                'name' => strtoupper($request->name),
                'email' => strtolower($request->email),
        ]);
        }
        $userProfile = UserDetail::where('user_id',$id)->firstOrFail();
        $userProfile->update([
            'number' => $request->number,
            'country_id' => $request->country_id,
            'city_id' => $request->city_id,
            'btc_address' => $request->btc_address,
        ]);
        return redirect('admin/users')->with('success','Data updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        User::destroy($id);
        alert()->success('User deleted!', 'Success')->persistent("Close");
        return redirect('admin/users');
    }

    public function viewSecurity()
    {
        return view('admin.users.changeAdminPass');
    }

    public function changeSecurity(Request $request)
    {
        $this->validate($request,[
            'current_password' => 'required',
            'password' => 'required|string|min:6|confirmed'
        ]);
        $id = Auth::User()->id;
        $requestData = $request->all();
        $user = User::where('id',$id)->first();
        $oldPassword = $user->password;
        $currentPassword = $requestData['current_password'];
        if(Hash::check($currentPassword, $oldPassword))
        {
            $newPassword = Hash::make($requestData['password']);
            try
            {
                $user->update([
                    'password' => $newPassword
                ]);
                return redirect()->back()->with('success','Password updated successfully');
            }
            catch (\Illuminate\Database\QueryException $e)
            {
                return redirect()->back()->withInput()->with('error',$e->getMessage());
            }
        }
        else
        {
            return redirect()->back()->withInput()->with('error','Current Password is Incorrect');
        }

    }

    public function exportUserData()
    {
        return Excel::download(new ExportData, 'Modinaama-users-data.xlsx');
    }
}
