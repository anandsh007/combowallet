<?php

namespace App\Http\Controllers\Admin;
use App\User;
use App\UserProfile;
use App\GiveHelp;
use App\GetHelp;
use App\Setting;
use App\UserPreStatus;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class ActionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $username = User::pluck('user_name','id');
        $requestData = $request->get('user_id');
        if(!empty($requestData))
        {
            $user = User::where('id', $requestData)->first();
            return view('admin.actions.block', compact('user','username'));
        }
        else
        {
            $user = null;
            return view('admin.actions.block', compact('username','user'));
        }

    }

    public function adminAction(Request $request)
    {
        $requestData = $request->all();
        if($requestData['action'] == "Block"){
            User::where('id',$requestData['id'])->update([
                'is_blocked' => true
            ]);
        }else{
            User::where('id',$requestData['id'])->update([
                'is_blocked' => false
            ]);
        }
        return redirect()->route('action.index')->with('success','Status changed successfully');
    }
}
