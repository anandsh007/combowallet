<?php
namespace App\Http\Controllers\Admin;

use App\User;
use App\UserDetail;
use App\GiveHelp;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
class JoiningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $requestData = $request->all();

        if(!empty($requestData))
        {
            $startDate = date('Y-m-d H:i:s',strtotime($requestData['start_date']));
            $endDate = date('Y-m-d H:i:s',strtotime($requestData['end_date']));
            $users = User::with('userDetails')
                     ->whereDate('created_at', '>=', $startDate)
                     ->whereDate('created_at', '<=', $endDate)
                     ->orderBy('created_at', 'DESC')  
                     ->get();
            return view('admin.joining.index',compact('users'));
        }     
        else
        {
            $users = null;
            return view('admin.joining.index',compact('users'));
        }
    }

    public function newJoining()
    {
        $users = User::with('userDetails','giveHelps')
                        ->real()
                        ->pending()
                        ->get();
        return view('admin.joining.newJoining',compact('users'));
    }

    public function registeredList()
    {
        $newUsers = User::with('userDetails','userSetting','giveHelps')->real()->get();
        $users = [];
        foreach($newUsers as $item)
        {
            if(count($item->giveHelps) == 0)
            {
                if($item->userSetting->account_status == 'inactive')
                {
                    $users[] = $item->toArray();
                }
            }
        }
        return view('admin.joining.registered',compact('users'));
    }
}
