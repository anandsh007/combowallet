<?php

namespace App\Http\Controllers\Auth;

use App\City;
use App\Country;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\SecretQuestion;
use App\User;
use App\UserDetail;
use App\UserNetwork;
use App\UserPassword;
use App\UserWallet;
use App\Mail\RegistrationMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use JsValidator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /*
    * Define your validation rules in a property in
    * the controller to reuse the rules.
    */
    protected $validationRules = [
        'sponsor_id' => 'required|exists:users,user_name',
        'sponsor_name' => 'required',
        'name' => ['required','string','min:3','max:41','regex:/(^[a-zA-Z\\s]*$)/u'],
        'email' => 'required|email:rfc,dns|unique:users,email',
        'user_name' => 'required|string|min:3|max:255|alpha_num|unique:users|regex:/^[a-zA-Z]{3}[A-Z0-9a-z]*$/',
        'number' => 'required|numeric|digits_between:5,10|unique:user_details,number',
        'country_id' => 'required',
        'city_id' => 'required',
        'sec_que_id' => 'required',
        'sec_que_ans' => 'required',
        'password' => 'required|string|min:6|confirmed'
    ];
    protected $customAttributes = [
        'sponsor_id' => 'Sponsor ID',
        'sponsor_name' => 'Sponsor Name',
        'name' => 'Name',
        'email' => 'Email',
        'user_name' => 'User ID',
        'number' => 'Mobile Number',
        'country_id' => 'Country',
        'city_id' => 'City',
        'password' => 'Password',
        'sec_que_id' => 'Secret Question',
        'sec_que_ans' => 'Secret Answer'
    ];

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::USER_DASHBOARD;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm(Request $request)
    {
        $sponsorDetails = null;
        if($request->has('ref')){
            $sponsorId = base64_decode($request->ref);
            $sponsorDetails = User::where('user_name',$sponsorId)->first();
        }
        $validator = JsValidator::make($this->validationRules, [],$this->customAttributes);
        $countries = Country::select('id','name')->get();
        $secretQuestions = SecretQuestion::select('id','question')->get();
        return view('auth.register', compact('validator','countries','secretQuestions','sponsorDetails'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, $this->validationRules,[],$this->customAttributes);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
                'name' => strtoupper($data['name']),
                'email' => strtolower($data['email']),
                'user_name' => strtolower($data['user_name']),
                'sponsor_id' => $data['sponsor_id'],
                'password' => Hash::make($data['password']),
            ]);
        $userDetails = UserDetail::create([
            'user_id' => $user->id,
            'number' => $data['number'],
            'country_id' => $data['country_id'],
            'city_id' => $data['city_id'],
            'sec_que_id' => $data['sec_que_id'],
            'sec_que_ans' => strtoupper($data['sec_que_ans']),
        ]);
        UserPassword::create([
            'user_id' => $user->id,
            'password' => $data['password']
        ]);
        UserNetwork::create([
            'user_id' => $user->id
        ]);
        UserWallet::create([
            'user_id' => $user->id,
            'amount' => 0
        ]);
        Mail::to($data['email'])->send(new RegistrationMail($user));
        Session(['successReg' =>'Thank you for registration! A mail has been sent to your registered Email address.']);
        return $user;
    }
}
