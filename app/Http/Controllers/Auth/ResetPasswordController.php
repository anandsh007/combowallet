<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\SecretQuestion;
use App\User;
use App\UserDetail;
use App\UserPassword;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function restPassQue()
    {
        $secretQuestions = SecretQuestion::select('id','question')->get();
        return view('auth.passwords.secretQue',compact('secretQuestions'));
    }

    public function secretQue(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'user_name' => 'required',
            'sec_que_id' => 'required',
            'sec_que_ans' => 'required',
            'password' => 'required|string|min:6|confirmed'
        ],[],[
            'user_name' => 'Username',
            'sec_que_id' => 'Secret Question',
            'sec_que_ans' => 'Secret Question Answer',
            'password' => 'Password'
        ]);
        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation->errors());
        }
        $user = User::where('user_name',$request->user_name)->first();
        if($user){
            $userDetail = UserDetail::where('user_id',$user->id)->first();
            if($request->sec_que_ans == $userDetail->sec_que_ans){
                try{
                    User::whereId($user->id)->update([
                        'password' => Hash::make($request->password)
                    ]);
                    $userPassword = UserPassword::where('user_id', $user->id)->firstOrFail();
                    $userPassword->update([
                        'password' => $request->password
                    ]);
                    return back()->with('success','Password updated successfully!');
                }catch(\ Throwable $e){
                    return back()->with('error',$e->getMessage());
                }
            }
            return back()->with('error','Sercret question answer is incorrect');
        }else{
            return back()->with('error','Username is incorrect');
        }
    }
}
