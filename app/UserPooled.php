<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPooled extends Model
{
    const PENDING = 1, PAID =2;
    protected $table = 'user_pooled';

    protected $guarded = [];
}
