<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserIncome extends Model
{
    const CREDIT = 1, DEBIT = 2;
    const NON_WORKING = 1, LEVEL_INCOME = 2, TRANSFERRED = 3, WITHDRAW = 4, TOP_PUP = 5, DIRECT_INCOME = 6, POOL_INCOME = 7, ADMIN = 8, ROYALTY_INCOME = 9, GLOBAL_INCOME = 10;
    const GROWTH_INCOME_WALLET = 1, DIRECT_INCOME_WALLET = 2, LEVEL_INCOME_WALLET = 3, MAIN_WALLET = 4, POOL_INCOME_WALLET = 5, TOP_UP_WALLET = 6,ROYALTY_INCOME_WALLET = 7, USER_WALLET = 8, GLOBAL_INCOME_WALLET = 9;
    const PENDING = 1, PAID = 2, REJECTED =3;
    protected $table = 'user_incomes';

    protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    protected function getGrowthIncome($userId){
        return $this->where('user_id',$userId)
            ->where('transaction_type',UserIncome::CREDIT)
            ->where('income_type',UserIncome::NON_WORKING)
            ->sum('amount');
    }

    protected function transferredGrowthIncome($userId){
        return $this->where('user_id',$userId)
        ->where('transaction_type',UserIncome::DEBIT)
        ->where('income_type',UserIncome::TRANSFERRED)
        ->where('source',UserIncome::GROWTH_INCOME_WALLET)
        ->sum('amount');
    }

    protected function getTotalDirectIncome($userId){
        return $this->where('user_id',$userId)
        ->where('transaction_type',UserIncome::CREDIT)
        ->where('income_type',UserIncome::DIRECT_INCOME)
        ->sum('amount');
    }

    protected function transferredDirectIncome($userId){
        return $this->where('user_id',$userId)
        ->where('transaction_type',UserIncome::DEBIT)
        ->where('income_type',UserIncome::TRANSFERRED)
        ->where('source',UserIncome::DIRECT_INCOME_WALLET)
        ->sum('amount');
    }

    protected function totalLevelIncome($userId){
        return $this->where('user_id',$userId)
        ->where('transaction_type',UserIncome::CREDIT)
        ->where('income_type',UserIncome::LEVEL_INCOME)
        ->sum('amount');
    }

    protected function transferredLevelIncome($userId){
        return $this->where('user_id',$userId)
        ->where('transaction_type',UserIncome::DEBIT)
        ->where('income_type',UserIncome::TRANSFERRED)
        ->where('source',UserIncome::LEVEL_INCOME_WALLET)
        ->sum('amount');
    }

    protected function totalPoolIncome($userId){
        return $this->where('user_id',$userId)
        ->where('transaction_type',UserIncome::CREDIT)
        ->where('income_type',UserIncome::POOL_INCOME)
        ->sum('amount');
    }

    protected function transferredPoolIncome($userId){
        return $this->where('user_id',$userId)
        ->where('transaction_type',UserIncome::DEBIT)
        ->where('income_type',UserIncome::TRANSFERRED)
        ->where('source',UserIncome::POOL_INCOME_WALLET)
        ->sum('amount');
    }

    protected function totalRoyaltyIncome($userId){
        return $this->where('user_id',$userId)
        ->where('transaction_type',UserIncome::CREDIT)
        ->where('income_type',UserIncome::ROYALTY_INCOME)
        ->sum('amount');
    }

    protected function transferredRoyaltyIncome($userId){
        return $this->where('user_id',$userId)
        ->where('transaction_type',UserIncome::DEBIT)
        ->where('income_type',UserIncome::TRANSFERRED)
        ->where('source',UserIncome::ROYALTY_INCOME_WALLET)
        ->sum('amount');
    }

    protected function totalGlobalLineIncome($userId){
        return $this->where('user_id',$userId)
        ->where('transaction_type',UserIncome::CREDIT)
        ->where('income_type',UserIncome::GLOBAL_INCOME)
        ->sum('amount');
    }

    protected function transferredGlobalLineIncome($userId){
        return $this->where('user_id',$userId)
        ->where('transaction_type',UserIncome::DEBIT)
        ->where('income_type',UserIncome::TRANSFERRED)
        ->where('source',UserIncome::GLOBAL_INCOME_WALLET)
        ->sum('amount');
    }
}
