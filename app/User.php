<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    const REGISTERED = 1, ACTIVE = 2, INACTIVE = 3, BLOCKED = 4, POOLED = 5;
    const USER = 1, ADMIN = 2;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','user_name','sponsor_id','activation_date','status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userDetails(){
        return $this->hasOne('App\UserDetail','user_id');
    }

    public function incomes(){
        return $this->hasMany('App\UserIncome','user_id');
    }

    public function userPassword(){
        return $this->hasOne('App\UserPassword','user_id');
    }

    /**
     * Functions
     */

     protected function getDirectTeam($id){
        $userName = $this->whereId($id)->pluck('user_name')->first();
        return $this->where('sponsor_id',$userName)->get();
     }

     public function scopeActive(){
         return $this->where('status',User::ACTIVE);
     }

     public function network(){
         return $this->hasOne('App\UserNetwork','user_id');
     }
}
