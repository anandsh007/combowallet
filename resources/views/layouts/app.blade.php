<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>Global Combo Wallet</title>
	<meta charset="UTF-8">
	<meta name="description" content="Global Combo Wallet">
	<meta name="keywords" content="loans, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Favicon -->
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="{{ asset('app/css/bootstrap.min.css') }}"/>
	<link rel="stylesheet" href="{{ asset('app/css/font-awesome.min.css') }}"/>
	<link rel="stylesheet" href="{{ asset('app/css/owl.carousel.min.css') }}"/>
	<link rel="stylesheet" href="{{ asset('app/css/flaticon.css') }}"/>
	<link rel="stylesheet" href="{{ asset('app/css/slicknav.min.css') }}"/>

	<!-- Main Stylesheets -->
	<link rel="stylesheet" href="{{ asset('app/css/style.css').'?v='.time() }}"/>
    @yield('styles')

	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js')}}"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js')}}"></script>
	<![endif]-->

</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Header Section -->
	<header class="header-section">
		<a href="{{url('/')}}" class="site-logo">
			<img src="{{asset('app/img/logo4.png')}}" alt="">
		</a>
		<nav class="header-nav">
			<ul class="main-menu">
                <li><a href="{{url('/')}}" class="active">Home</a></li>
                <li><a href="{{url('/plan')}}">Combo Wallet System</a></li>
                <li><a href="{{url('/policy')}}">Combo Wallet Policy</a></li>
                <li><a href="{{url('/legal')}}">Legal</a></li>
                <li><a href="{{url('/contact')}}">Reach Us</a></li>
			</ul>
			<div class="header-right">
                @guest
                <a href="{{url('login')}}" class="hr-btn"><i class="flaticon-024-laptop"></i>Login</a>
                <a href="{{url('register')}}" class="hr-btn"><i class="flaticon-024-laptop"></i>Register</a>
                @else
                    @if(Auth::User()->role == \App\User::ADMIN)
                    <a href="{{url('admin/dashboard')}}" class="hr-btn dashboardUrl"><i class="flaticon-024-laptop"></i>{{Auth::User()->name}}</a>
                    @else
                    <a href="{{url('user/dashboard')}}" class="hr-btn dashboardUrl"><i class="flaticon-024-laptop"></i>{{Auth::User()->name}}</a>
                    @endif
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {!! Form::token() !!}
                    </form>
                    <a class="hr-btn" href="{{url('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
                @endguest
			</div>
		</nav>
	</header>
	<!-- Header Section end -->
    @yield('content')
	<!-- Footer Section -->
	<footer class="footer-section">
		<div class="container">
            <a href="{{url('/')}}" class="footer-logo">
				<img src="{{asset('app/img/logo4.png')}}" alt="">
			</a>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tem por incididunt ut labore et dolore mag na aliqua.  Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse potenti. Ut gravida mattis magna, non varius lorem sodales nec. In libero orci, ornare non nisl.</p>
			<div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
		</div>
	</footer>
	<!-- Footer Section end -->

	<!--====== Javascripts & Jquery ======-->
    <script src="{{ asset('app/js/jquery-3.2.1.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="{{ asset('app/js/bootstrap.min.js')}}"></script>
	<script src="{{ asset('app/js/jquery.slicknav.min.js')}}"></script>
	<script src="{{ asset('app/js/owl.carousel.min.js')}}"></script>
	<script src="{{ asset('app/js/jquery-ui.min.js')}}"></script>
	<script src="{{ asset('app/js/main.js')}}"></script>
    @yield('scripts')
    @guest
    <script>
        var loginUrl = "{{url('login')}}";
        var registerUrl = "{{url('register')}}"
        var html = '<li><a href="'+loginUrl+'" class="btn btn-danger mt-4">Login</a></li>\
                    <li><a href="'+registerUrl+'" class="btn btn-success mt-4">Register</a></li>';
        $('.slicknav_nav').append(html);
    </script>
    @else
    <script>
    var name = "{{Auth::User()->name}}";
    var dashboardUrl = $('.dashboardUrl').attr('href');
    var logoutUrl = "{{url('user/logout')}}"
    var html = '<li><a href="'+dashboardUrl+'" class="btn btn-success mt-4">'+name+'</a></li>\
                <li><a href="'+logoutUrl+'" class="btn btn-danger mt-4">Logout</a></li>';
    $('.slicknav_nav').append(html);
    </script>
    @endguest
	</body>
</html>
