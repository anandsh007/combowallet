<!-- Header-->
<header id="header" class="header">
    <div class="top-left">
        <div class="navbar-header">
        <a class="navbar-brand" href="{{url('user/dashboard')}}"><img src="{{asset('images/logo6.png')}}" alt="Logo"></a>
            <a class="navbar-brand hidden" href="./"><img src="{{asset('images/logo2.png')}}" alt="Logo"></a>
            <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
        </div>
    </div>
    <div class="top-right">
        <div class="header-menu">
            <div class="header-left">

                <div class="dropdown for-notification">
                    @if(Auth::User()->status == \App\User::ACTIVE)
                    <button class="btn btn-success">ACTIVE</button>
                    @elseif(Auth::User()->status == \App\User::INACTIVE)
                    <button class="btn btn-secondary">INACTIVE</button>
                    @elseif(Auth::User()->status == \App\User::REGISTERED)
                    <button class="btn btn-info">REGISTERED</button>
                    @elseif(Auth::User()->status == \App\User::BLOCKED)
                    <button class="btn btn-danger">BLOCKED</button>
                    @endif
                </div>

                <div class="dropdown for-message">
                    @if(Auth::User()->role == \App\User::ADMIN)
                    <a class="btn btn-danger" style="white-space: break-spaces" href="{{route('admin.logout')}}"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
                    @else
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                {!! Form::token() !!}
                    </form>
                    <a class="btn btn-danger" style="white-space: break-spaces" href="{{url('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
                    @endif
                </div>
            </div>

            <div class="user-area dropdown float-right">
                <button class="btn btn-success" style="white-space: break-spaces"><i class="fa fa-address-book" aria-hidden="true"></i> <strong>Name:</strong> {{Auth::User()->name}}</button>
                <button class="btn btn-info" style="white-space: break-spaces"><i class="fa fa-user-circle" aria-hidden="true"></i> <strong>Username:</strong> {{Auth::User()->user_name}}</button>
            </div>

        </div>
    </div>
</header>
<!-- /#header -->
