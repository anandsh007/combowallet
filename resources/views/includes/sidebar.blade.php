@if(Auth::User()->role == \App\User::USER)
<!-- Left Panel -->
<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{route('user.index')}}"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                </li>
                <li class="menu-title">User Links</li><!-- /.menu-title -->
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cogs"></i>Profiles</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-user"></i><a href="{{route('profile.viewProfile')}}">View Profile</a></li>
                        <li><i class="fa fa-user"></i><a href="{{route('profile.sponsorProfile')}}">Sponsor Profile</a></li>
                        <li><i class="fa fa-btc"></i><a href="{{route('profile.showBtcForm')}}">Update BTC Address</a></li>
                        <li><i class="fa fa-key"></i><a href="{{route('profile.showChangePassForm')}}">New Password</a></li>
                        <li><i class="fa fa-lock"></i><a href="{{route('profile.showNewPassForm')}}">Change Password</a></li>
                        <li><i class="fa fa-user"></i><a href="{{route('profile.activationHistory')}}">Activation History</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Team</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-user"></i><a href="{{route('team.directTeam')}}">Direct Team</a></li>
                        <li><i class="fa fa-users"></i><a href="{{route('team.totalTeam')}}">Total Team</a></li>
                        <li><i class="fa fa-user-plus"></i><a href="{{route('team.registeredTeam')}}">Registered Team</a></li>
                        <li><i class="fa fa-user-plus"></i><a href="{{route('team.network')}}">Network</a></li>
                        <li><i class="fa fa-user-plus"></i><a href="{{route('team.networkChart')}}">Network Chart</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Income</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-th"></i><a href="{{route('income.growthIncome')}}">Growth Income</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="{{route('income.directIncome')}}">Direct Income</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="{{route('income.levelIncome')}}">Level Income</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="{{route('income.poolIncome')}}">Pool Income</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="{{route('income.royaltyIncome')}}">Royalty Income</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="{{route('income.globalLineIncome')}}">Global Line Income</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Wallet</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-th"></i><a href="{{route('wallet.transfer')}}">Transfer Fund</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="{{route('wallet.showWithdrawForm')}}">Withdraw Fund</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="{{route('wallet.topupUserIdForm')}}">Top Up ID</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="{{route('wallet.userFundRequest')}}">Fund Request</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="{{route('wallet.withdrawalRequest')}}">Withdrawal Requests</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Message</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-th"></i><a href="#">ID To ID Message & Browse Upload</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>
@else
<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{route('admin.dashboard')}}"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                </li>
                <li class="menu-title">User Links</li><!-- /.menu-title -->
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cogs"></i>Profiles</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-user"></i><a href="{{url('admin/users')}}">Users</a></li>
                        <li><i class="fa fa-key"></i><a href="{{ route('user.viewSecurity') }}">Change Admin Password</a></li>

                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Downline</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-user-plus"></i><a href="{{route('downline.networkTree')}}">Tree View</a></li>
                        <li><i class="fa fa-user"></i><a href="{{route('downline.directTeam')}}">Total Direct Team</a></li>
                        <li><i class="fa fa-users"></i><a href="{{route('downline.totalDownline')}}">Total Downline</a></li>
                        <li><i class="fa fa-user-plus"></i><a href="{{route('downline.blockedMembers')}}">Total Blocked List</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('admin.withdrawalsList')}}"><i class="menu-icon fa fa-laptop"></i>Withdrawals </a>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Fund Management</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-th"></i><a href="{{route('fund.fundList')}}">Added Fund History</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="{{route('fund.addFundForm')}}">Add Fund To User</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="{{route('fund.fundRequest')}}">Fund request</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Admin Actions</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-th"></i><a href="{{ route('action.index') }}">Block/Unblock User</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="{{ url('/admin/news') }}">Publish News</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('admin.addcityForm')}}"><i class="menu-icon fa fa-laptop"></i>Add City </a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>
@endif
<!-- /#left-panel -->
