<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>{{$thirdLevel}}</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="{{route('user.index')}}">Dashboard</a></li>
                            <li><a href="#">{{$secLevel}}</a></li>
                            <li class="active">{{$thirdLevel}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
