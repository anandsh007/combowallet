@extends('layouts.app')

@section('content')
<!-- Page top Section end -->
<section class="page-top-section set-bg" data-setbg="app/img/page-top-bg/4.jpg">
    <div class="container">
        <h2>Combo Wallet Policy</h2>
        <nav class="site-breadcrumb">
            <a class="sb-item" href="#">Home</a>
            <span class="sb-item active">Combo Wallet Policy</span>
        </nav>
    </div>
</section>
<!-- Page top Section end -->
<!-- Loans Section end -->
<section class="loans-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <img src="{{asset('app/img/loans.jpg')}}" alt="">
            </div>
            <div class="col-lg-6">
                <div class="loans-text">
                    <h2>Our Terms and Condition</h2>
                    <ul>
                        <li>MINIMUM 3 ACTIVE ID MUST NEED FOR LEVEL INCOME.</li>
                        <li>RE-TOP UP MUST BE REQUIRED FOR ANY TRANSACTIONS.</li>
                        <li>ONLY REGISTERED ID WILL NOT GET ANY TREE PLACEMENT.</li>
                        <li>LEVEL INCOME CONDITION IS 1ST LEVEL=3 DIRECT ACTIVE ID,2ND LEVEL =4 DIRECT ACTIVE ID AND 3RD LEVEL=5 DIRECT ACTIVE ID.</li>
                        <li>EVERY WITHDRAWAL DEDUCTION 20% AS ADMIN CHARGE.</li>
                        <li>WITHDRAWAL WILL CREDIT IN ONLY BTC ACCOUNTS.</li>
                        <li>PASSWORD RECOVERY ONLY ON EMAIL OR BY SECRET ANSWER.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Loans Section end -->
@endsection
