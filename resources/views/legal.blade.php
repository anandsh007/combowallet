@extends('layouts.app')

@section('content')
<!-- Page top Section end -->
<section class="page-top-section set-bg" data-setbg="app/img/page-top-bg/4.jpg">
    <div class="container">
        <h2>Legal</h2>
        <nav class="site-breadcrumb">
            <a class="sb-item" href="#">Home</a>
            <span class="sb-item active">Legal</span>
        </nav>
    </div>
</section>
<!-- Page top Section end -->
<!-- Loans Section end -->
<section class="loans-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <img src="{{asset('images/cerf.jpg')}}" alt="">
            </div>
            <div class="col-lg-6">
                <div class="loans-text">
                    <h2>Company Information</h2>
                    <p>Global Combo Wallet is officially registered in the Thailand Company number 13751376. Global Combo Wallet gives an opportunity to make your future bright and gives you financial independence.</p>
                    <p> You can join , easy to get started and you don’t need any qualification. Earn money by completing simple tasks online. Everyone can join.
                    <p>It’s simply the job of grow your network. The plus point is that you can work from where ever you want.</p>
                    <p>We are sure that the right combination of the best world experience with a deep understanding of the realities of, electronic currency and financial environment is a key factor to success. Today we are satisfied by our position taken on the market and company growth.</p>
                    <p>The legality of the company's activities is confirmed by publicly available legal certificate.</p>
                    <p>Company Registration Number: 13751376</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Loans Section end -->
@endsection
