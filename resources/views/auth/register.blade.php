@extends('layouts.app')
@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
<style>
    .login-form label {
        font-weight: 900;
    }
    .error-help-block{
        color: red;
    }
    </style>
@endsection
@section('content')
<!-- Page top Section end -->
<section class="page-top-section set-bg" data-setbg="app/img/page-top-bg/4.jpg">
    <div class="container">
        <h2>User Register Area</h2>
        <nav class="site-breadcrumb">
            <a class="sb-item" href="#">Home</a>
            <span class="sb-item active">Register</span>
        </nav>
    </div>
</section>
<!-- Page top Section end -->
<section class="contact-section spad">
    <div class="container">
        <div class="row">
            <div class="offset-md-2 col-lg-6">
                <div class="login-form">
                    <form id="registerform" method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group">
                            <label>Sponsor ID</label>
                            <input type="text" name="sponsor_id" id="sponsorId" class="form-control" value="{{isset($sponsorDetails)?$sponsorDetails->user_name:''}}" placeholder="Sponsor ID">
                        </div>
                        <div class="form-group">
                            <label>Sponsor Name</label>
                            <input type="text" name="sponsor_name" id="sponsorName" class="form-control text-uppercase" value="{{isset($sponsorDetails)?$sponsorDetails->name:''}}" placeholder="Sponsor Name" readonly>
                        </div>
                        <div class="form-group">
                            <label>Create User ID</label>
                            <input type="text" name="user_name" class="form-control text-lowercase" placeholder="User ID">
                        </div>
                        <div class="form-group">
                            <label>Full Name</label>
                            <input type="text" name="name" class="form-control text-uppercase" placeholder="Full Name">
                        </div>
                        <div class="form-group">
                            <label>Email address</label>
                            <input type="email" name="email" class="form-control text-lowercase" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label>Country</label>
                            <select name="country_id" class="form-control selectpicker" id="countries" data-live-search="true">
                                <option value="">--Select--</option>
                                @foreach ($countries as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>City</label>
                            <select name="city_id" class="form-control selectpicker" id="cities" data-live-search="true">
                                <option value="">--Select--</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Mobile Number</label>
                            <input type="number" name="number" class="form-control" placeholder="Mobile Number">
                        </div>
                        <div class="form-group">
                            <label>Select Secret Question</label>
                            <select name="sec_que_id" class="form-control selectpicker">
                                <option value="">--Select--</option>
                                @foreach ($secretQuestions as $item)
                                <option value="{{$item->id}}">{{$item->question}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Answer</label>
                            <input type="text" name="sec_que_ans" class="form-control text-uppercase" placeholder="Answer of secret question">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password">
                        </div>
                        <button type="submit" class="btn btn-danger btn-flat m-b-30 m-t-30">Register</button>
                        <div class="register-link m-t-15 text-center">
                            <p>Already have account ? <a href="{{url('/login')}}"> Sign in</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator->selector('#registerform') !!}
<script src="{{asset('js/custom.js')}}"></script>
@endsection
