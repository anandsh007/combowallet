@extends('layouts.app')

@section('content')
<!-- Page top Section end -->
<section class="page-top-section set-bg" data-setbg="/app/img/page-top-bg/4.jpg">
    <div class="container">
        <h2>Forget Password</h2>
        <nav class="site-breadcrumb">
            <a class="sb-item" href="#">Home</a>
            <span class="sb-item active">Forget Password</span>
        </nav>
    </div>
</section>
<div class="container mt-4 mb-4">
    <div class="row">
        <div class="offset-md-2 col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="user_name" class="col-md-4 col-form-label text-md-right">Username</label>

                            <div class="col-md-6">
                                <input id="user_name" type="text" class="form-control @error('user_name') is-invalid @enderror" value="{{ old('user_name') }}" required autocomplete="user_name" autofocus>

                                @error('user_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                            <div class="col-md-6">
                                <label class="emailText col-form-label">------</label>
                                <input id="email" type="hidden" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" readonly autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="button" class="btn btn-primary sendResetMail">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                        <div class="register-link m-t-15 text-center">
                            <p>Reset your password using secret question ? <a href="{{url('reset-password/secret-que')}}"> Click Here</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('#user_name').change(function(){
        $('#email').val('');
        $('.emailText').text('------');
        var userName = $(this).val();
        var url = "{{url('/')}}"+'/get/user-details/'+userName;
        $.get(url,function(response){
            if(response.status == 'ok'){
                $('#email').val(response.email);
                $('.emailText').text(response.maskedEmail)
            }
        });
    });
    $('.sendResetMail').click(function(){
        if($('#email').val() != '' && $('#email').val() != undefined){
            $(this).closest('form').submit();
        }else{
            alert('Mail is Invalid');
        }
    });
</script>
@endsection
