@extends('layouts.app')

@section('content')
<!-- Page top Section end -->
<section class="page-top-section set-bg" data-setbg="/app/img/page-top-bg/4.jpg">
    <div class="container">
        <h2>Reset Password</h2>
        <nav class="site-breadcrumb">
            <a class="sb-item" href="#">Home</a>
            <span class="sb-item active">Reset Password</span>
        </nav>
    </div>
</section>
<div class="container mt-4 mb-4">
    <div class="row">
        <div class="offset-md-2 col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @include('includes.messages')
                    <form method="POST" action="{{ route('password.secretQue') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="user_name" class="col-md-4 col-form-label text-md-right">Username</label>

                            <div class="col-md-6">
                                <input id="user_name" type="text" class="form-control @error('user_name') is-invalid @enderror" name="user_name" value="{{ old('user_name') }}" required autocomplete="user_name" autofocus>

                                @error('user_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="sec_que_id" class="col-md-4 col-form-label text-md-right">Select Secret Question</label>
                            <div class="col-md-6">
                                <select name="sec_que_id" class="form-control @error('sec_que_id') is-invalid @enderror selectpicker">
                                    <option value="">--Select--</option>
                                    @foreach ($secretQuestions as $item)
                                    <option value="{{$item->id}}">{{$item->question}}</option>
                                    @endforeach
                                </select>
                                @error('sec_que_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="sec_que_ans" class="col-md-4 col-form-label text-md-right">Enter Answer</label>
                            <div class="col-md-6">
                                <input id="sec_que_ans" type="text" class="form-control @error('sec_que_ans') is-invalid @enderror" name="sec_que_ans" value="{{ old('sec_que_ans') }}" required>
                                @error('sec_que_ans')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">New Password</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password_confirmation" class="col-md-4 col-form-label text-md-right">Confirm Password</label>
                            <div class="col-md-6">
                                <input id="password_confirmation" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" value="{{ old('password_confirmation') }}" required>
                                @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('#user_name').change(function(){
        $('#email').val('');
        var userName = $(this).val();
        var url = "{{url('/')}}"+'/get/user-details/'+userName;
        $.get(url,function(response){
            if(response.status == 'ok'){
                $('#email').val(response.email);
            }
        });
    });
    $('.sendResetMail').click(function(){
        if($('#email').val() != '' && $('#email').val() != undefined){
            $(this).closest('form').submit();
        }else{
            alert('Mail is Required');
        }
    });
</script>
@endsection
