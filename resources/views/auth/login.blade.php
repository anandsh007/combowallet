@extends('layouts.app')

@section('styles')
<style>
.login-form label {
    font-weight: 900;
}
.error-help-block{
    color: red;
}
</style>
@endsection

@section('content')
<!-- Page top Section end -->
<section class="page-top-section set-bg" data-setbg="app/img/page-top-bg/4.jpg">
    <div class="container">
        <h2>User Login Area</h2>
        <nav class="site-breadcrumb">
            <a class="sb-item" href="#">Home</a>
            <span class="sb-item active">Login</span>
        </nav>
    </div>
</section>
<!-- Page top Section end -->
<section class="contact-section spad">
    <div class="container">
        <div class="row">
            <div class="offset-md-2 col-lg-6">
                <div class="login-form">
                    @include('includes.messages')
                    <form id="loginform" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <label>Username</label>
                            <input id="user_name" type="text" class="form-control @error('user_name') is-invalid @enderror" name="user_name" value="{{ old('user_name') }}" required autocomplete="user_name" autofocus>
                            @error('user_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                            </label>
                            <label class="pull-right">
                                <a href="{{ route('password.request') }}">Forgotten Password?</a>
                            </label>

                        </div>
                        <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Sign in</button>
                        <div class="register-link m-t-15 text-center">
                            <p>Don't have account ? <a href="{{url('register')}}"> Sign Up Here</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator->selector('#loginform') !!}
@endsection
