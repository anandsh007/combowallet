@extends('layouts.backend')
@section('styles')
<style>
    #tree {
    width: 100%;
    height: 100%;
}
</style>
@endsection
@section('content')
@include('includes.breadcrumb',['secLevel' => 'Team','thirdLevel' => 'Network'])
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Network</strong>
                    </div>
                    <div class="card-body">
                        <div class="container">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/downline/network-tree', 'class' => '','role' => 'search'])  !!}
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            {!! Form::select('user_name',$username,null,('required' == 'required') ? ['class' =>'form-control', 'required' => 'required','placeholder'=> 'Select Username'] : ['class' => 'form-control','placeholder'=> 'Select Username']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Show', ['class' => 'btn btn-success p-2 m-1']) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div id="tree"></div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
@section('scripts')
<script src="https:///balkangraph.com/js/latest/OrgChart.js"></script>
<script>
window.onload = function () {
    var nodes = {!! $jsonData !!};


    for (var i = 0; i < nodes.length; i++) {
        nodes[i].field_number_children = childCount(nodes[i].id);
    }

    function childCount(id) {
        let count = 0;
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].pid == id) {
                count++;
                count += childCount(nodes[i].id);
            }
        }

        return count;
    }

    OrgChart.templates.rony.field_number_children = '<circle cx="60" cy="110" r="15" fill="#F57C00"></circle><text fill="#ffffff" x="60" y="115" text-anchor="middle">{val}</text>';

    var chart = new OrgChart(document.getElementById("tree"), {
        template: "rony",
        collapse: {
            level: 3
        },
        nodeBinding: {
            field_0: "Name",
            field_1: "Username",
            field_2: "SponsorId",
            field_3: "Country",
            img_0: "img",
            field_number_children: "field_number_children"
        },
        nodes: nodes
    });
};
</script>
@endsection
