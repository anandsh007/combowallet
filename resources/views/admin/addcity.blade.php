@extends('layouts.backend')

@section('content')
@include('includes.breadcrumb',['secLevel' => 'Add City','thirdLevel' => 'Admin'])
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body offset-md-2 col-md-8">
                                <div class="card-title">
                                    <h3 class="text-center">Add City</h3>
                                </div>
                                @include('includes.messages')
                                <form id="cityForm" action="{{route('admin.addCity')}}" method="post" novalidate="novalidate">
                                    @csrf
                                    <div class="form-group">
                                        <label for="country_id" class="control-label mb-1">Country</label>
                                        <select name="country_id" class="form-control selectpicker" data-live-search="true">
                                            @foreach ($countries as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="city" class="control-label mb-1">City</label>
                                        <input id="city" name="city" type="text" class="form-control" aria-required="true" aria-invalid="false" placeholder="Enter City">
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-lg btn-info btn-block">
                                            <i class="fa fa-lock fa-lg"></i>&nbsp; Submit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div> <!-- .card -->

            </div><!--/.col-->
        </div>
    </div>
</div>
@endsection
