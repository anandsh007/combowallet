@extends('layouts.backend')
@section('content')
@include('includes.breadcrumb',['secLevel' => 'Profiles','thirdLevel' => 'Edit User '.$user->id])
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Edit User #{{$user->id}}</strong>
                    </div>
                    @include('includes.messages')
                    <div class="card-body">
                        {!! Form::model($user, [
                        'method' => 'PATCH',
                        'url' => ['/admin/users', $user->id],
                        'class' => 'form-horizontal'
                        ]) !!}
                        @include ('admin.users.form', ['submitButtonText' => 'Update'])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
@section('scripts')
<script src="{{asset('js/custom.js')}}"></script>
@endsection
