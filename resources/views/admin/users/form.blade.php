<h5 class="card-title text-uppercase font-weight-bold">Personal Details</h5>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4">Name
                <span class="required">*</span>
            </label>
            <div class="col-md-12">
                <input id="name" type="text" class="form-control custom-input text-style1" name="name" value="{{ $user->name }}" placeholder="Full Name As Per Bank Details" required autofocus>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('user_name') ? ' has-error' : '' }}">
            <label for="user_name" class="col-md-4">User Name
                <span class="required">*</span>
            </label>
            <div class="col-md-12">
                <input id="user_name" type="text" class="form-control custom-input text-style2" name="user_name" value="{{ $user->user_name }}" placeholder="Only Characters & Numbers are Allowed" required autofocus readonly="readonly">
                @if ($errors->has('user_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('user_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('state_id') ? ' has-error' : '' }}">
            <label for="state_id" class="col-md-6">Country
                <span class="required">*</span>
            </label>
            <div class="col-md-12">
                <select class="form-control custom-input" name="country_id" id="countries" required="">
                    <option value="">--Select--</option>
                    @foreach ($countries as $item)
                    <option value="{{$item->id}}" {{$user->userDetails->country_id == $item->id?'selected':''}}>{{$item->name}}</option>
                    @endforeach
                </select>
                @if ($errors->has('country_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('country_id') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('city_id') ? ' has-error' : '' }}">
            <label for="district_id" class="col-md-6">City
                <span class="required">*</span>
            </label>
            <div class="col-md-12">
                <select name="city_id" class="form-control" id="cities">
                    <option value=""><-- Select City --></option>
                    @foreach($cities as $city)
                        <option value="{{$city->id}}" {{$city->id == $user->userDetails['city_id'] ? 'selected':''}}>{{$city->name}}</option>
                    @endforeach
                </select>
                @if ($errors->has('city_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('city_id') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('mob_no') ? ' has-error' : '' }}">
            <label for="mobile" class="col-md-6">Mobile
                <span class="required">*</span>
            </label>
            <div class="col-md-12">
                <input id="mobile" type="text" class="form-control custom-input" name="number" value="{{ $user->userDetails['number'] }}" placeholder="10 Digit Numeric Only" required>

                @if ($errors->has('number'))
                    <span class="help-block">
                        <strong>{{ $errors->first('number') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="mobile" class="col-md-6">Email
                <span class="required">*</span>
            </label>
            <div class="col-md-12">
                <input id="email" type="email" class="form-control custom-input" name="email" value="{{ $user->email }}" placeholder="Email Address" required>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('btc_address') ? ' has-error' : '' }}">
            <label for="btc_address" class="col-md-6">BTC Address
            </label>
            <div class="col-md-12">
                <input id="btc_address" type="text" class="form-control custom-input" name="btc_address" value="{{ $user->userDetails['btc_address'] }}" placeholder="BTC Address">

                @if ($errors->has('btc_address'))
                    <span class="help-block">
                        <strong>{{ $errors->first('btc_address') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-md-6">Password
                <span class="required">*</span>
            </label>

            <div class="col-md-12">
                <input id="password" type="password" class="form-control custom-input" name="password"
                       placeholder="PASSWORD">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label for="password-confirm" class="col-md-6">Confirm Password
                <span class="required">*</span>
            </label>
            <div class="col-md-12">
                <input id="password-confirm" type="password" class="form-control custom-input"
                       name="password_confirmation" placeholder="CONFIRM PASSWORD">
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn btn-success']) !!}
    </div>
</div>

