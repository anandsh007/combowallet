@extends('layouts.backend')
@section('styles')
<link rel="stylesheet" href="{{asset('assets/css/lib/datatable/dataTables.bootstrap.min.css')}}">
@endsection
@section('content')
@include('includes.breadcrumb',['secLevel' => 'Profiles','thirdLevel' => 'Users'])
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Users</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr No</th>
                                    <th>Name</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Number</th>
                                    <th>City</th>
                                    <th>Country</th>
                                    <th>Sponsor ID</th>
                                    <th>Joining Date</th>
                                    <th>Password</th>
                                    <th>Secret Question</th>
                                    <th>Secret Answer</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $member)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$member->name}}</td>
                                    <td>{{$member->user_name}}</td>
                                    <td>{{$member->email}}</td>
                                    <td>{{$member->userDetails->number}}</td>
                                    <td>{{$member->userDetails->city->name}}</td>
                                    <td>{{$member->userDetails->country->name}}</td>
                                    <td>{{$member->sponsor_id}}</td>
                                    <td>{{$member->created_at->format('d, M Y')}}</td>
                                    <td>{{isset($member->userPassword)?$member->userPassword->password:'N/A'}}</td>
                                    <td>{{$member->userDetails->secretQuestion->question}}</td>
                                    <td>{{$member->userDetails->sec_que_ans}}</td>
                                    <td>
                                        @if($member->status == \App\User::REGISTERED)
                                        REGISTERED
                                        @elseif($member->status == \App\User::ACTIVE)
                                        ACTIVE
                                        @elseif($member->status == \App\User::INACTIVE)
                                        INACTIVE
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('/admin/users/' . $member->id . '/edit') }}" title="Edit User"><button class="btn btn-secondary btn-sm"><i class="fa fa-edit"></i> Edit</button></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
@section('scripts')
    <script src="{{asset('assets/js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/js/init/datatables-init.js')}}"></script>


    <script type="text/javascript">
        $(document).ready(function() {
        $('#bootstrap-data-table-export').DataTable();
    } );
    </script>
@endsection
