@extends('layouts.backend')

@section('content')
@include('includes.breadcrumb',['secLevel' => 'Profiles','thirdLevel' => 'Change Admin Password'])
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Change Admin Password</strong>
                    </div>
                    @include('includes.messages')
                    <form method="POST" action="{{ route('user.changeSecurity') }}" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group {{ $errors->has('current_password') ? ' has-error' : '' }}">
                            <label for="current_password" class="col-md-6">Current Password
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input id="current_password" type="password" class="form-control custom-input" name="current_password" placeholder="CURRENT PASSWORD" required="required">
                                @if ($errors->has('current_password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('current_password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-6">New Password
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input id="password" type="password" class="form-control custom-input" name="password" placeholder="NEW PASSWORD" required="required">
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password-confirm" class="col-md-6">Confirm Password
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input id="password-confirm" type="password" class="form-control custom-input" name="password_confirmation" placeholder="CONFIRM PASSWORD" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-4">
                                <button class="form-submit btn btn-success" type="submit" onclick="return confirm('Confirm update?');">Update Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
