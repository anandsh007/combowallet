@extends('layouts.backend')
@section('content')
@include('includes.breadcrumb',['secLevel' => 'Admin Actions','thirdLevel' => 'Edit News # '.$news->type])
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">News</strong>
                    </div>
                    <div class="card-body">
                        <a href="{{ url('/admin/news') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />
                        @include('includes.messages')
                        {!! Form::model($news, [
                            'method' => 'PATCH',
                            'url' => ['/admin/news', $news->id],
                            'class' => 'form-horizontal',
                            'files' => true
                            ]) !!}
                            @include ('admin.news.form', ['submitButtonText' => 'Update'])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection

