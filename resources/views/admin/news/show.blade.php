@extends('layouts.backend')
@section('content')
@include('includes.breadcrumb',['secLevel' => 'Admin Actions','thirdLevel' => 'News '.$news->type])
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">News</strong>
                    </div>
                    <div class="card-body">
                        <a href="{{ url('/admin/news') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/news/' . $news->id . '/edit') }}" title="Edit News"><button class="btn btn-primary btn-sm"><i class="fas fa-edit"></i> Edit</button></a>
                        <br/>
                        <br/>
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <tbody>
                                <tr>
                                    <th>ID</th><td>{{ $news->id }}</td>
                                </tr>
                                <tr><th> Subject </th><td> {{ $news->subject }} </td></tr><tr><th> Details </th><td> {{ $news->details }} </td></tr><tr><th> Type </th><td> {{ $news->type }} </td></tr><tr><th> Updated Date </th><td> {{ $news->updated_at }} </td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
