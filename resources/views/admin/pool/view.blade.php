@extends('layouts.backend')
@section('styles')
    <style>
        .section {
            padding-top: 40px;
            padding-bottom: 40px;
        }
        p{
            margin-bottom: 0rem;
        }
        .rounded-circle {
            border-radius: 50%!important;
            width: 40px;
            height: 40px;
            text-align: -webkit-center;
            padding-top: 8px;
            border: 3px solid #ffffff!important;
        }
    </style>
@endsection
@section('content')
    @include('partials.header')
    <h2 class="text-center">Pooled Users</h2>
    <div class="body">
        <div class="row">
        @foreach($pooledUsers as $item)
            <!-- blog post -->
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="body bg-red" style="min-height: 230px;">
                            <p><h5 class="card-title border border-success rounded-circle">{{$loop->iteration}}</h5></p>
                            <div class="row">
                                <div class="col-md-4">
                                    Username:
                                </div>
                                <div class="col-md-8">
                                    {{$item->user->user_name}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    Name:
                                </div>
                                <div class="col-md-8">
                                    {{$item->user->name}}
                                </div>
                            </div>
                            <a href="#" class="btn btn-warning btn-sm">Pool Amount: {{$item->amount}}</a>
                        </div>
                    </div>
                </div>
                <!-- blog post -->
            @endforeach
        </div>
    </div>
    @include('partials.footer')
@endsection