@extends('layouts.backend')

@section('content')
    @include('partials.header')
    <h2 class="text-center">Pooled Out List</h2>
    <div class="body">
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
            <thead>
            <tr>
                <th>Sr No.</th>
                <th>Username</th>
                <th>Name</th>
                <th>Mobile Number</th>
                <th>Email</th>
                <th>Amount</th>
                <th>Exit date</th>
            </tr>
            </thead>
            <tbody>
            @php
                $i = 1;
            @endphp
            @foreach($pooledOutUsers as $item)
                <tr>
                    <td>{{$i}}</td>
                    <td>{{$item->user->user_name}}</td>
                    <td>{{$item->user->name}}</td>
                    <td>{{$item->user->userdetails->mob_no}}</td>
                    <td>{{$item->user->email}}</td>
                    <td>{{$item->amount}}</td>
                    <td>{{$item->created_at->format('d, M Y h:i:s A')}}</td>
                </tr>
                @php
                    $i =$i+1;
                @endphp
            @endforeach
            </tbody>
        </table>
    </div>
    </div>
    @include('partials.footer')
@endsection
