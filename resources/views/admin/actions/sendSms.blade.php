@extends('layouts.backend')
@section('styles')
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #182b45;
        }
    </style>
@endsection
@section('content')
    @include('partials.header')
    <h2 class="text-center heading-color">Send SMS</h2>
    <div class="body">
        <form action="{{route('action.sendSms')}}" method="post" class="form-horizontal">
            @csrf
            <div class="form-group">
                <label class="col-md-4">Select Users</label>
                <div class="col-md-8">
                    <select class="" name="mob_no[]" required multiple id="user">
                        @foreach($users as $user)
                        <option value="{{$user->userDetails->mob_no}}">{{$user->name}}({{$user->user_name}})</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-8 mt-3">
                    <div class="demo-checkbox">
                        <input type="checkbox" id="selectAll" />
                        <label for="selectAll">Select All</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4">Message</label>
                <div class="col-md-8">
                    <textarea class="" name="message" placeholder="Enter Messase" required style="height: 200px!important;"></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-8">
                    <button type="submit" class="btn btn-success m-2">Send </button>
                </div>
            </div>
        </form>
    </div>
    @include('partials.footer')
@endsection
@section('scripts')
    <script>
        $("#selectAll").click(function(){
            if($("#selectAll").is(':checked') ){
                $("#user").find('option').prop("selected",true);
                $("#user").trigger('change');
            }else{
                $("#user").find('option').prop("selected",false);
                $("#user").trigger('change');
            }
        });
    </script>
@endsection