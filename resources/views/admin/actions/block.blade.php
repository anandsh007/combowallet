@extends('layouts.backend')
@section('styles')
@endsection
@section('content')
@include('includes.breadcrumb',['secLevel' => 'Admin Action','thirdLevel' => 'Block/Unblock User'])
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Block/Unblock User</strong>
                    </div>
                    <div class="card-body">
                        @include('includes.messages')
                        <div class="container">
                            {!! Form::open(['method' => 'GET', 'route' => 'action.index', 'class' => ''])  !!}
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <div class="col-md-12" style="margin-bottom: 0px">
                                            {!! Form::select('user_id',$username,null,('required' == 'required') ? ['class' => 'form-control form-line', 'required' => 'required', 'placeholder' => 'Select Username'] : ['class' => 'form-line form-control', 'placeholder' => 'Select Username']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Show', ['class' => 'btn btn-success p-1']) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile Number</th>
                                    <th>Date of Joining</th>
                                    <th>Sponsor ID</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            @if(!is_null($user))
                            <tbody>
                            <tr>
                                <td>{{ $user->user_name }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->userDetails->number }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>{{ $user->sponsor_id }}</td>
                                <td>
                                    {!! Form::open(['method' => 'GET', 'route' => 'action.adminAction', 'class' => 'form-inline '])  !!}
                                    <input type="hidden" name="id" value="{{ $user->id }}">
                                    @if($user->is_blocked == true)
                                        <input type="submit" name="action" class="btn btn-success" value="Unblock">
                                    @else
                                        <input type="submit" name="action" class="btn btn-success" value="Block">
                                    @endif
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            </tbody>
                        @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
