@extends('layouts.backend')
@section('styles')
<link rel="stylesheet" href="{{asset('assets/css/lib/datatable/dataTables.bootstrap.min.css')}}">
@endsection
@section('content')
@include('includes.breadcrumb',['secLevel' => 'Withdrawals','thirdLevel' => 'Withdrawals'])
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Withdrawals List</strong>
                    </div>
                    <div class="card-body">
                        <div class="container">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/withdrawals', 'class' => '','role' => 'search'])  !!}
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <select name="status" class="form-control" onchange="javascript:$(this).closest('form').submit();">
                                                <option value="" {{Request::get('status') == ""?'selected':''}}>--Select--</option>
                                                <option value="1" {{Request::get('status') == "1"?'selected':''}}>Pending</option>
                                                <option value="2" {{Request::get('status') == "2"?'selected':''}}>Paid</option>
                                                <option value="3" {{Request::get('status') == "3"?'selected':''}}>Rejected</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        @include('includes.messages')
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr No</th>
                                    <th>Name</th>
                                    <th>User Name</th>
                                    <th>BTC</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <th>Requested Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($withdrawals as $member)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$member->user->name}}</td>
                                    <td>{{$member->user->user_name}}</td>
                                    <td>{{$member->user->userDetails->btc_address}}</td>
                                    <td>{{$member->amount}}</td>
                                    <td>
                                        @if($member->status == \App\UserIncome::PENDING)
                                        Pending
                                        @elseif($member->status == \App\UserIncome::PAID)
                                        Paid
                                        @elseif($member->status == \App\UserIncome::REJECTED)
                                        Rejected
                                        @endif
                                    </td>
                                    <td>{{$member->created_at->format('d, M Y')}}</td>
                                    <td>
                                        @if($member->status == \App\UserIncome::PENDING)
                                        <a class="btn btn-success" href="{{route('admin.withdrawalsPaid',['id' => $member->id])}}">PAY</a>
                                        <a class="btn btn-danger" href="{{route('admin.withdrawalsRejected',['id' => $member->id])}}">REJECT</a>
                                        @elseif($member->status == \App\UserIncome::PAID)
                                        <a class="btn btn-success disabled" href="javascript:void(0)">PAID</a>
                                        @elseif($member->status == \App\UserIncome::REJECTED)
                                        <a class="btn btn-danger disabled" href="javascript:void(0)">REJECTED</a>
                                        @endif

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
@section('scripts')
    <script src="{{asset('assets/js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/js/init/datatables-init.js')}}"></script>


    <script type="text/javascript">
        $(document).ready(function() {
        $('#bootstrap-data-table-export').DataTable();
    } );
    </script>
@endsection
