@extends('layouts.backend')
@section('styles')
<link rel="stylesheet" href="{{asset('assets/css/lib/datatable/dataTables.bootstrap.min.css')}}">
@endsection
@section('content')
<!-- Content -->
<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
        <!-- Widgets  -->
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-1">
                                <i class="ti-pie-chart"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="count">{{$totalSystemId}}</span></div>
                                    <div class="stat-heading">TOTAL SYSTEM ID:</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-2">
                                <i class="pe-7s-cash"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text">$<span class="count">{{$totalSystemFund}}</span></div>
                                    <div class="stat-heading">TOTAL SYSTEM FUND:</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card"  data-toggle="modal" data-target="#registeredModal">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-1">
                                <i class="ti-pie-chart"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="count">{{$totalNewId}}</span></div>
                                    <div class="stat-heading">TOTAL NEW ID:</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-2">
                                <i class="pe-7s-cash"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text">$<span class="count">{{$totalTopupFund}}</span></div>
                                    <div class="stat-heading">TOTAL TOPUP FUND:</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-1">
                                <i class="ti-pie-chart"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="count">{{$totalActiveId}}</span></div>
                                    <div class="stat-heading">TOTAL ACTIVE ID:</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-2">
                                <i class="pe-7s-cash"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text">$<span class="count">{{$totalReTopupFund}}</span></div>
                                    <div class="stat-heading">TOTAL RE-TOPUP FUND:</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-1">
                                <i class="ti-pie-chart"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="count">{{$totalInActiveId}}</span></div>
                                    <div class="stat-heading">TOTAL INACTIVE ID:</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-2">
                                <i class="pe-7s-cash"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text">$<span class="count">{{$totalBalanceFund}}</span></div>
                                    <div class="stat-heading">TOTAL BALANCE FUND:</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card" data-toggle="modal" data-target="#blockedModal">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-1">
                                <i class="ti-pie-chart"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="count">{{$totalBlockedId}}</span></div>
                                    <div class="stat-heading">TOTAL BLOCKED ID:</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-2">
                                <i class="pe-7s-cash"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text">$<span class="count">{{$totalAddedFund}}</span></div>
                                    <div class="stat-heading">TOTAL ADDED FUND:</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="card" data-toggle="modal" data-target="#royaltyModal">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-1">
                                <i class="ti-pie-chart"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="count">{{$totalAchiever}}</span></div>
                                    <div class="stat-heading">TOTAL ROYALTY ACHIEVER:</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-2">
                                <i class="pe-7s-cash"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text">$<span class="count">{{$royaltyFund}}</span></div>
                                    <div class="stat-heading">TOTAL ROYALTY FUND:</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Widgets -->
    </div>
    <!-- .animated -->
</div>
<!-- /.content -->

<!-- Blocked List Modal -->
<div class="modal fade" id="blockedModal" tabindex="-1" role="dialog" aria-labelledby="blockedModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="blockedModalLabel">Blocked User</h5>
        </div>
        <div class="modal-body">
            <table id="blocked-list" class="table table-striped table-bordered table-responsive">
                <thead>
                    <tr>
                        <th>Sr No</th>
                        <th>Name</th>
                        <th>User Name</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>City</th>
                        <th>Country</th>
                        <th>Blocked Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($blockedLists as $member)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$member->name}}</td>
                        <td>{{$member->user_name}}</td>
                        <td>{{$member->userDetails->number}}</td>
                        <td>{{$member->email}}</td>
                        <td>{{$member->userDetails->city->name}}</td>
                        <td>{{$member->userDetails->country->name}}</td>
                        <td>{{$member->updated_at->format('d, M Y')}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<!-- Royalty List Modal -->
<div class="modal fade" id="royaltyModal" tabindex="-1" role="dialog" aria-labelledby="royaltyModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="royaltyModalLabel">Royalty Achiever</h5>
        </div>
        <div class="modal-body">
            <table id="royalty-list" class="table table-striped table-bordered table-responsive">
                <thead>
                    <tr>
                        <th>Sr No</th>
                        <th>Name</th>
                        <th>User Name</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>City</th>
                        <th>Country</th>
                        <th>Joined Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($royaltyLists as $member)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$member->name}}</td>
                        <td>{{$member->user_name}}</td>
                        <td>{{$member->userDetails->number}}</td>
                        <td>{{$member->email}}</td>
                        <td>{{$member->userDetails->city->name}}</td>
                        <td>{{$member->userDetails->country->name}}</td>
                        <td>{{$member->created_at->format('d, M Y')}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<!-- Royalty List Modal -->
<div class="modal fade" id="registeredModal" tabindex="-1" role="dialog" aria-labelledby="registeredModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="registeredModalLabel">Registered User</h5>
        </div>
        <div class="modal-body">
            <table id="registered-list" class="table table-striped table-bordered table-responsive">
                <thead>
                    <tr>
                        <th>Sr No</th>
                        <th>Name</th>
                        <th>User Name</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>City</th>
                        <th>Country</th>
                        <th>Joined Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($registeredLists as $member)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$member->name}}</td>
                        <td>{{$member->user_name}}</td>
                        <td>{{$member->userDetails->number}}</td>
                        <td>{{$member->email}}</td>
                        <td>{{$member->userDetails->city->name}}</td>
                        <td>{{$member->userDetails->country->name}}</td>
                        <td>{{$member->created_at->format('d, M Y')}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="{{asset('assets/js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/js/init/datatables-init.js')}}"></script>


    <script type="text/javascript">
        $(document).ready(function() {
        $('#blocked-list,#royalty-list,#registered-list').DataTable();
    } );
    </script>
@endsection

