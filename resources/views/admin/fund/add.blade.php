@extends('layouts.backend')

@section('content')
@include('includes.breadcrumb',['secLevel' => 'Fund Management','thirdLevel' => 'Add Fund To User'])
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body offset-md-2 col-md-8">
                                <div class="card-title">
                                    <h3 class="text-center">Add Fund to User</h3>
                                </div>
                                @include('includes.messages')
                                <form id="transferForm" novalidate="novalidate">
                                    <div class="form-group">
                                        <label for="user_id" class="control-label mb-1">User ID</label>
                                        <input id="user_id" name="user_id" type="text" class="form-control" aria-required="true" aria-invalid="false" placeholder="Enter User ID" data-parsley-required="true" data-parsley-remote={{route('helper.validateUserId')}}>
                                    </div>
                                    <div>
                                        <button type="button" class="btn btn-lg btn-info btn-block nextBtn">
                                            <i class="fa fa-lock fa-lg"></i>&nbsp; Submit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div> <!-- .card -->

            </div><!--/.col-->
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="transferModal" tabindex="-1" role="dialog" aria-labelledby="transferModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-body">
            <div class="col-md-12">
                <section class="card">
                    <div class="twt-feed blue-bg">
                        <div class="corner-ribon black-ribon">
                            <i class="fa fa-twitter"></i>
                        </div>
                        <div class="fa fa-twitter wtt-mark"></div>
                        <div class="media">
                            <a href="#">
                                <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="{{asset('images/admin.jpg')}}">
                            </a>
                            <div class="media-body">
                                <h2 class="text-white display-6 name">Jim Doe</h2>
                                <p class="text-light userName">Project Manager</p>
                                <p class="text-light email">Test@gmsil.com</p>
                            </div>
                        </div>
                    </div>
                    <div class="weather-category twt-category">
                        {{-- <ul>
                            <li class="active">
                                <h5>750</h5>
                                Tweets
                            </li>
                            <li>
                                <h5>865</h5>
                                Following
                            </li>
                            <li>
                                <h5>3645</h5>
                                Followers
                            </li>
                        </ul> --}}
                    </div>
                    <div class="error-message"></div>
                    <div class="twt-write col-sm-12">
                        <input type="hidden" name="user_id" id="userId">
                        <input id="amount" placeholder="Enter amount"class="form-control" name="amount" data-parsley-required="true" data-parsley-type="number" data-parsley-min="1">
                    </div>
                    <footer class="twt-footer">

                    </footer>
                </section>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary submitBtn">Transfer Fund</button>
        </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('.nextBtn').click(function(){
        var form = $('#transferForm');
        form.parsley().whenValidate().done(function() {
            var userId = $('#user_id').val();
            $('#loader').show();
            $.get("{{url('get/user-details')}}/"+userId,function(response){
                $('#loader').hide();
                $('#userId').val(response.userId);
                $('.name').text(response.name);
                $('.email').text(response.email);
                $('.userName').text(response.userName);
                $('#transferModal').modal('show');
            },'json');
        });
    });

    $('.submitBtn').click(function(){
        $('#amount').parsley().validate();
        if($('#amount').parsley().isValid()){
            var amount = $('#amount').val();
            var userId = $('#userId').val();
            var formData = {};
            formData['amount'] = amount;
            formData['userId'] = userId;
            formData['_token'] = "{{ csrf_token() }}";
            $('#loader').show();
            $.post("{{route('fund.addFund')}}",formData,function(response){
                $('#loader').hide();
                if(response.status == 'ok'){
                    var successHtml = '<div class="alert alert-success alert-block">\
                                            <button type="button" class="close" data-dismiss="alert">×</button>\
                                                <strong>'+response.message+'</strong>\
                                        </div>';
                    $('.error-message').html(successHtml);
                    $('#amount').val('');
                    $('#user_id').val('');
                }else{
                    var successHtml = '<div class="alert alert-danger alert-block">\
                                            <button type="button" class="close" data-dismiss="alert">×</button>\
                                                <strong>'+response.message+'</strong>\
                                        </div>';
                    $('.error-message').html(successHtml);
                }
            },'json')
        }
    });
</script>
@endsection
