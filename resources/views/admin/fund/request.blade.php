@extends('layouts.backend')
@section('styles')
<link rel="stylesheet" href="{{asset('assets/css/lib/datatable/dataTables.bootstrap.min.css')}}">
@endsection
@section('content')
@include('includes.breadcrumb',['secLevel' => 'Fund Management','thirdLevel' => 'Fund Request'])
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Fund Request</strong>
                    </div>
                    <div class="card-body">
                        @include('includes.messages')
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr No</th>
                                    <th>Name</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Mobile Number</th>
                                    <th>Country</th>
                                    <th>Amount</th>
                                    <th>Remark</th>
                                    <th>Proof</th>
                                    <th>Status</th>
                                    <th>Requested Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($fundRequests as $member)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$member->user->name}}</td>
                                    <td>{{$member->user->user_name}}</td>
                                    <td>{{$member->user->email}}</td>
                                    <td>{{$member->user->userDetails->number}}</td>
                                    <td>{{$member->user->userDetails->country->name}}</td>
                                    <td>{{$member->amount}}</td>
                                    <td>{{$member->remark}}</td>
                                    <td><button class="btn btn-success seeProof" data-image="{{asset('uploads/proof-files/'.$member->proof)}}" data-toggle="modal" data-target="#proofModal">Click</button></td>
                                    <td>
                                        {!! Form::open(['method' => 'GET', 'route' => 'fund.changeFundRequestStatus', 'class' => '','role' => 'search'])  !!}
                                        <div class="row clearfix">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="hidden" name="requestId" value="{{$member->id}}">
                                                    <input type="hidden" name="userId" value="{{$member->user_id}}">
                                                    <div class="form-line">
                                                        <select name="action" class="form-control" onchange="javascript:$(this).closest('form').submit();">
                                                            <option value="{{\App\FundRequest::PENDING}}"  {{$member->status == \App\FundRequest::PENDING ? 'selected':''}}>Pending</option>
                                                            <option value="{{\App\FundRequest::PAID}}"  {{$member->status == \App\FundRequest::PAID ? 'selected':''}}>Paid</option>
                                                            <option value="{{\App\FundRequest::REJECTED}}"  {{$member->status == \App\FundRequest::REJECTED ? 'selected':''}}>Rejected</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </td>
                                    <td>{{$member->created_at->format('d, M Y')}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->
<div class="modal fade" id="proofModal" tabindex="-1" role="dialog" aria-labelledby="proofModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-body">
            <img src="" id="image" class="img-responsive">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script src="{{asset('assets/js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/js/init/datatables-init.js')}}"></script>


    <script type="text/javascript">
        $(document).ready(function() {
        $('#bootstrap-data-table-export').DataTable();
    } );

    $('.seeProof').click(function(){
        var src = $(this).data('image');
        $('#image').attr('src',src);
    })
    </script>
@endsection
