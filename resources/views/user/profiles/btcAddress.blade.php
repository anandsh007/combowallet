@extends('layouts.backend')

@section('content')
@include('includes.breadcrumb',['secLevel' => 'Profiles','thirdLevel' => 'BTC Address'])
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body offset-md-2 col-md-8">
                                <div class="card-title">
                                    <h3 class="text-center">Bitcoin Address</h3>
                                </div>
                                @include('includes.messages')
                                <form id="btcForm" action="{{route('profile.updateBtcAddress')}}" method="post" novalidate="novalidate">
                                    @csrf
                                    <div class="form-group">
                                        <label for="btc_address" class="control-label mb-1">BTC Address</label>
                                        <input id="btc_address" name="btc_address" type="text" class="form-control" aria-required="true" aria-invalid="false" placeholder="Enter Bitcoin Address" value="{{$userDetail->btc_address}}" required="required" {{isset($userDetail->btc_address)?'readonly':''}}>
                                    </div>
                                    <div>
                                    <button type="{{isset($userDetail->btc_address)?'button':'submit'}}" class="btn btn-lg btn-info btn-block {{isset($userDetail->btc_address)?'disabled':''}}">
                                            <i class="fa fa-lock fa-lg"></i>&nbsp; Submit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div> <!-- .card -->

            </div><!--/.col-->
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('#btcForm').parsley();
</script>
@endsection
