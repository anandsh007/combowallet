@extends('layouts.backend')

@section('content')
@include('includes.breadcrumb',['secLevel' => 'Profiles','thirdLevel' => 'Change Password'])
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body offset-md-2 col-md-8">
                                <div class="card-title">
                                    <h3 class="text-center">Change Password</h3>
                                </div>
                                @include('includes.messages')
                                <form id="passwordForm" action="{{route('profile.newPassword')}}" method="post" novalidate="novalidate">
                                    @csrf
                                    <div class="form-group">
                                        <label for="current_password" class="control-label mb-1">Current Password</label>
                                        <input id="current_password" name="current_password" type="password" class="form-control" aria-required="true" aria-invalid="false" placeholder="Enter you current password">
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="password" class="control-label mb-1">New Password</label>
                                                <input id="password" name="password" type="password" class="form-control" placeholder="Enter new password">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label for="password_confirmation" class="control-label mb-1">Confirm Password</label>
                                            <div class="input-group">
                                                <input id="password_confirmation" name="password_confirmation" type="password" class="form-control" autocomplete="off" placeholder="Confirm new password">
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-lg btn-info btn-block">
                                            <i class="fa fa-lock fa-lg"></i>&nbsp; Submit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div> <!-- .card -->

            </div><!--/.col-->
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator->selector('#passwordForm') !!}
@endsection
