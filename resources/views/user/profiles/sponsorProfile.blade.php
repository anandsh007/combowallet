@extends('layouts.backend')

@section('content')
@include('includes.breadcrumb',['secLevel' => 'Profiles','thirdLevel' => 'Sponsor Profile'])
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <aside class="profile-nav alt">
                    <section class="card">
                        <div class="card-header user-header alt bg-dark">
                            <div class="media">
                                <a href="#">
                                    <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="{{asset('images/admin.jpg')}}">
                                </a>
                                <div class="media-body">
                                    <h2 class="text-light display-6">{{$userDetail->name}}</h2>
                                    <p>USER ID: {{$userDetail->user_name}}</p>
                                </div>
                            </div>
                        </div>


                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <a href="#"> <i class="fa fa-envelope-o"></i> {{$userDetail->email}}</a>
                            </li>
                            <li class="list-group-item">
                                <a href="#"> <i class="fa fa-phone-square"></i> {{$userDetail->userDetails->number}} </a>
                            </li>
                            <li class="list-group-item">
                                <a href="#"> <i class="fa fa-globe"></i> {{$userDetail->userDetails->country->name}} </a>
                            </li>
                            <li class="list-group-item">
                                <a href="#"> <i class="fa fa-home"></i> {{$userDetail->userDetails->city->name}} </a>
                            </li>
                        </ul>

                    </section>
                </aside>
            </div>
        </div>
    </div>
</div>
@endsection
