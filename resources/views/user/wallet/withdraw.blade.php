@extends('layouts.backend')

@section('content')
@include('includes.breadcrumb',['secLevel' => 'Wallet','thirdLevel' => 'Withdraw Fund'])
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="offset-md-2 col-md-8">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="stat-widget-five">
                                            <div class="stat-icon dib flat-color-1">
                                                <i class="pe-7s-cash"></i>
                                            </div>
                                            <div class="stat-content">
                                                <div class="text-left dib">
                                                    <div class="stat-text">$<span class="count" id="wallet">{{$walletAmount}}</span></div>
                                                    <div class="stat-heading">Wallet</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body offset-md-2 col-md-8">
                                <div class="card-title">
                                    <h3 class="text-center">Withdraw Fund</h3>
                                </div>
                                @include('includes.messages')
                                <form id="withdrawForm" method="post" action="{{route('wallet.withdrawFund')}}" novalidate="novalidate">
                                    @csrf
                                    <div class="form-group">
                                        <label for="amount" class="control-label mb-1">Enter Amount</label>
                                        <input id="amount" name="amount" type="number" class="form-control" data-parsley-required="true" data-parsley-type="number" data-parsley-min="10" data-parsley-max="50">
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-lg btn-info btn-block nextBtn">
                                            <i class="fa fa-lock fa-lg"></i>&nbsp; Submit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div> <!-- .card -->

            </div><!--/.col-->
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('#withdrawForm').parsley();
</script>
@endsection
