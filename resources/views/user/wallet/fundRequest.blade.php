@extends('layouts.backend')
@section('styles')
<link rel="stylesheet" href="{{asset('css/dropzone.css')}}">
@endsection
@section('content')
@include('includes.breadcrumb',['secLevel' => 'Wallet','thirdLevel' => 'Fund Request'])
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="offset-md-2 col-md-8">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="stat-widget-five">
                                            <div class="stat-icon dib flat-color-1">
                                                <i class="pe-7s-cash"></i>
                                            </div>
                                            <div class="stat-content">
                                                <div class="text-left dib">
                                                    <div class="stat-text">$<span class="count" id="wallet">{{$userWallet->top_up_wallet}}</span></div>
                                                    <div class="stat-heading">Topup Wallet</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body offset-md-2 col-md-8">
                                <div class="card-title">
                                    <h3 class="text-center">Request Fund</h3>
                                    <p class="text-success">PLEASE SEND PROOF OF PAYMENT TO ADMIN FOR FUND GIVEN BTC ADDRESS AND WAIT SOMETIME</p>
                                </div>
                                @include('includes.messages')
                                <div class="error-message"></div>
                                <div class="alert alert-success" role="alert">
                                    <strong>BTC Address:</strong>&nbsp;{{config('global.admin_bitcoinaddr')}}
                                </div>
                                <div class="form-group">
                                    <label>Amount</label>
                                    <input type="number" id="requestAmount" name="amount" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Remark</label>
                                    <textarea id="remark" name="remark" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <form action="{{url('/file-upload')}}" class="dropzone" id="my-awesome-dropzone">
                                        @csrf
                                        <input type="hidden" name="user_id" value="{{Auth::User()->id}}">
                                    </form>
                                    <input type="hidden" name="proofFile" id="proofFile">
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-primary" id="proofSubmit">Confirm</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .card -->
            </div><!--/.col-->
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('js/dropzone.js')}}"></script>
<script>
    Dropzone.options.myAwesomeDropzone = {
        maxFiles: 1,
        success: function(file, response) {
            $('#proofFile').val(response.file);
        },
        init: function() {
            this.on("maxfilesexceeded", function(file){
                alert("No more files please!");
            });
        }
    };

    $('#proofSubmit').click(function(){
        $('.error-message').empty();
        var formData = {};
        formData['file'] = $('#proofFile').val();
        formData['amount'] = $('#requestAmount').val();
        formData['remark'] = $('#remark').val();
        if(formData['amount'] != '' && formData['file'] != ''){
            $('#loader').show();
            $.get("{{route('fundRequest')}}",formData,function(response){
                $('#loader').hide();
                if(response.status == 'ok'){
                    alert("Request sent successfully");
                    window.location.reload();
                }else{
                    var html = '<div class="alert alert-danger">\
                    <strong>Error: </strong>'+response.message+'\
                    </div>';
                    $('.error-message').html(html);
                }
            },'json');
        }else{
            alert("Amount and Proof field is required");
        }
    })
</script>
<script>
    $('#topUpForm').parsley();
</script>
@endsection
