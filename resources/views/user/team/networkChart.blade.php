@extends('layouts.backend')
@section('styles')
<link rel="stylesheet" href="{{asset('assets/css/lib/datatable/dataTables.bootstrap.min.css')}}">
@endsection
@section('content')
@include('includes.breadcrumb',['secLevel' => 'Team','thirdLevel' => 'Network Chart'])
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Network Chart</strong>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Level</th>
                                    <th>Total Members</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $level1 = 0;
                                    $level2 = 0;
                                    $level3 = 0;
                                @endphp
                                @foreach ($team as $member)
                                @if($member['level'] == 1)
                                @php
                                    $level1 = $level1 + 1;
                                @endphp
                                @elseif($member['level'] == 2)
                                @php
                                    $level2 = $level2 + 1;
                                @endphp
                                @elseif($member['level'] == 3)
                                @php
                                    $level3 = $level3 + 1;
                                @endphp
                                @endif
                                @endforeach
                                <tr>
                                    <td>1</td>
                                    <td>{{$level1}}</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>{{$level2}}</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>{{$level3}}</td>
                                </tr>
                            </tbody>
                        </table>
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr No</th>
                                    <th>Level</th>
                                    <th>Sponsor ID</th>
                                    <th>Sponsor Name</th>
                                    <th>Name</th>
                                    <th>User Name</th>
                                    <th>City</th>
                                    <th>Country</th>
                                    <th>Status</th>
                                    <th>Joining Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($team as $member)
                                @if($member['id'] != Auth::User()->id && $member['level'] <= '3')
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$member['level']}}</td>
                                    <td>{{$member['sponsor_id']}}</td>
                                    <td>{{$member['sponsor_name']}}</td>
                                    <td>{{$member['name']}}</td>
                                    <td>{{$member['user_name']}}</td>
                                    <td>{{$member['city']}}</td>
                                    <td>{{$member['country']}}</td>
                                    <td>{{$member['status']}}</td>
                                    <td>{{$member['joining_date']}}</td>
                                </tr>
                                @php
                                    $i = $i + 1;
                                @endphp
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
@section('scripts')
    <script src="{{asset('assets/js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/js/init/datatables-init.js')}}"></script>


    <script type="text/javascript">
        $(document).ready(function() {
        $('#bootstrap-data-table-export').DataTable();
    } );
    </script>
@endsection
