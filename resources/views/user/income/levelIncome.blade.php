@extends('layouts.backend')

@section('content')
@include('includes.breadcrumb',['secLevel' => 'Income','thirdLevel' => 'Level Income'])
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="row">
                                <div class="offset-md-2 col-md-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="stat-widget-five">
                                                <div class="stat-icon dib flat-color-1">
                                                    <i class="pe-7s-cash"></i>
                                                </div>
                                                <div class="stat-content">
                                                    <div class="text-left dib">
                                                        <div class="stat-text">$<span class="count" id="wallet">{{$totalWalletAmount}}</span></div>
                                                        <div class="stat-heading">Total Level Income</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="stat-widget-five">
                                                <div class="stat-icon dib flat-color-1">
                                                    <i class="pe-7s-cash"></i>
                                                </div>
                                                <div class="stat-content">
                                                    <div class="text-left dib">
                                                        <div class="stat-text">$<span class="count" id="walletAvailableAmount">{{$availableAmount}}</span></div>
                                                        <div class="stat-heading">Available Level Income</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body offset-md-2 col-md-8">
                                <div class="card-title">
                                    <h3 class="text-center">Transfer Fund To Wallet</h3>
                                </div>
                                @include('includes.messages')
                                <form id="transferForm" method="post" action="{{route('income.transferFund')}}" novalidate="novalidate">
                                    @csrf
                                    <div class="form-group">
                                        <label for="transferred_wallet_type" class="control-label mb-1">Wallet Type</label>
                                        <select id="transferred_wallet_type" name="transferred_wallet_type" class="form-control" data-parsley-required="true">
                                            <option value="{{\App\UserIncome::TOP_UP_WALLET}}">Topup Wallet</option>
                                            <option value="{{\App\UserIncome::MAIN_WALLET}}">Main Wallet</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="source" value="{{\App\UserIncome::LEVEL_INCOME_WALLET}}">
                                        <label for="amount" class="control-label mb-1">Enter Amount</label>
                                        <input id="amount" name="amount" type="number" class="form-control" data-parsley-required="true" data-parsley-type="number" data-parsley-min="1">
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-lg btn-info btn-block nextBtn">
                                            <i class="fa fa-lock fa-lg"></i>&nbsp; Submit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div> <!-- .card -->

            </div><!--/.col-->
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('#transferForm').parsley();
</script>
@endsection
