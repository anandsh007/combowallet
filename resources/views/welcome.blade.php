@extends('layouts.app')

@section('styles')
<style>
.error-help-block{
    color: red;
}
</style>
@endsection
@section('content')
<!-- Hero Section end -->
<section class="hero-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="hs-text">
                    <h2>{{$news->subject}}</h2>
                    <marquee class="news" direction="up" onmouseover="this.stop()" onmouseout="this.start()" scrollamount="3">
                    <p>{{$news->details}}</p>
                    </marquee>
                    <a href="{{url('plan')}}" class="site-btn sb-dark">Know More</a>
                </div>
            </div>
            {{-- <div class="col-lg-6">
                <form class="hero-form" id="loginform" method="POST" action="{{ route('login') }}">
                    @csrf
                    <input id="user_name" type="text" class="form-control @error('user_name') is-invalid @enderror" name="user_name" value="{{ old('user_name') }}" required autocomplete="user_name" placeholder="Your Username">
                    @error('user_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Your Password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </p>
                    <button class="site-btn">Login!</button>
                </form>
            </div> --}}
        </div>
    </div>
    <div class="hero-slider owl-carousel">
        <div class="hs-item set-bg" data-setbg="app/img/hero-slider/1.jpg"></div>
        <div class="hs-item set-bg" data-setbg="app/img/hero-slider/2.jpg"></div>
        <div class="hs-item set-bg" data-setbg="app/img/hero-slider/3.jpg"></div>
    </div>
</section>
<!-- Hero Section end -->
	<!-- Why Section end -->
	<section class="why-section spad">
		<div class="container">
			<div class="text-center mb-5 pb-4">
				<h2>Why Choose us?</h2>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="icon-box-item">
						<div class="ib-icon">
							<i class="flaticon-012-24-hours"></i>
						</div>
						<div class="ib-text">
							<h5>Money in 1 Hour!</h5>
							<p>Lorem ipsum dolor sit amet, consecte-tur adipiscing elit, sed do eiusmod tem por incididunt ut labore et dolore mag na aliqua. </p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="icon-box-item">
						<div class="ib-icon">
							<i class="flaticon-036-customer-service"></i>
						</div>
						<div class="ib-text">
							<h5>Helpfull Staff</h5>
							<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per incep-tos himenaeos. Suspendisse potenti. Ut gravida mattis.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="icon-box-item">
						<div class="ib-icon">
							<i class="flaticon-039-info"></i>
						</div>
						<div class="ib-text">
							<h5>Credit History Considered</h5>
							<p>Conubia nostra, per inceptos himenae os. Suspendisse potenti. Ut gravida mattis magna, non varius lorem sodales nec. In libero orci.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="text-center pt-3">
                <a href="{{url('register')}}" class="site-btn sb-big">Register Now!</a>
			</div>
		</div>
	</section>
	<!-- Why Section end -->


	<!-- CTA Section end -->
	<section class="cta-section set-bg" data-setbg="img/cta-bg.jpg')}}">
		<div class="container">
			<h2>Already have a <strong>Combo Wallet</strong> Account?</h2>
			<h5>If you're thinking about borrowing more, we're here to help.</h5>
			<a href="{{url('login')}}" class="site-btn sb-dark sb-big">Login Now!</a>
		</div>
	</section>
	<!-- CTA Section end -->

	<!-- Info Section -->
	<section class="info-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-5">
					<img src="{{ asset('app/img/info-img.jpg')}}" alt="">
				</div>
				<div class="col-lg-7">
					<div class="info-text">
						<h2>We’re here to help</h2>
						<h5>Monday to Thursday (8am to 8pm), and Friday (8am to 5pm).</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tem por incididunt ut labore et dolore mag na aliqua.  Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse potenti. Ut gravida mattis magna, non varius lorem sodales nec. In libero orci, ornare non nisl.</p>
						<ul>
							<li>+34 56873 2246</li>
							<li>support@combowallet.info</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Info Section end -->
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator->selector('#loginform') !!}
@endsection
