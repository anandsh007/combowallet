@extends('layouts.app')

@section('content')
<!-- Page top Section end -->
<section class="page-top-section set-bg" data-setbg="app/img/page-top-bg/4.jpg">
    <div class="container">
        <h2>Combo Wallet System</h2>
        <nav class="site-breadcrumb">
            <a class="sb-item" href="#">Home</a>
            <span class="sb-item active">System Plan</span>
        </nav>
    </div>
</section>
<!-- Page top Section end -->
<section class="elements-section spad">
    <div class="container">
        <div class="element">
            <h3 class="el-title">Combo Wallet System</h3>
            <div class="row">
                <div class="col-md-8">
                    <div class="icon-box-item">
                        <div class="ib-icon">
                            <i class="flaticon-039-info"></i>
                        </div>
                        <div class="ib-text">
                            <h5>REGISTRATION IS FREE ONLY SINGLE ID IS POSSIBLE BY SAME DETAILS IN THIS SYSTEM.</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="icon-box-item">
                        <div class="ib-icon">
                            <i class="flaticon-039-info"></i>
                        </div>
                        <div class="ib-text">
                            <h5>TOP UP AMOUNT-10$ AND RE-TOP UP AMOUNT-15$ AFTER 3 LEVELS CONTINUE.</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="icon-box-item">
                        <div class="ib-icon">
                            <i class="flaticon-039-info"></i>
                        </div>
                        <div class="ib-text">
                            <h5>NONWORKING GROWTH INCOME 1$ PER DAY UPTO 20 DAYS ONLY.</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="icon-box-item">
                        <div class="ib-icon">
                            <i class="flaticon-039-info"></i>
                        </div>
                        <div class="ib-text">
                            <h5>DIRECT INCOME 20% BY EVERY DIRECT ID TOP UP AND RE-TOP UP AMOUNT BY UNLIMITED REFERRALS,1ST LEVEL INCOME-4$,2ND LEVEL INCOME-18$ AND 3RD LEVEL INCOME 60$ MEANS TOTAL LEVEL INCOME-82$ ONLY.</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="icon-box-item">
                        <div class="ib-icon">
                            <i class="flaticon-039-info"></i>
                        </div>
                        <div class="ib-text">
                            <h5>MINIMUM WITHDRAWAL 10$ AND MAXIMUM 50$ PER DAY 8:00-9:00 AM ADMIN CHARGE DEDUCTION 10% EVERY WITHDRAWAL.</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="icon-box-item">
                        <div class="ib-icon">
                            <i class="flaticon-039-info"></i>
                        </div>
                        <div class="ib-text">
                            <h5>MINIMUM 3 DIRECT MUST BE REQUIRED FOR LEVEL INCOME AND EVERY DIRECT MUST BE ACTIVE FOR LEVEL INCOME.</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="icon-box-item">
                        <div class="ib-icon">
                            <i class="flaticon-039-info"></i>
                        </div>
                        <div class="ib-text">
                            <h5>THIS IS 3X3 MATRIX AUTO FILLING SYSTEM TOP TO BOTTOM AND LEFT TO RIGHT ALL OVER WORLD.</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="icon-box-item">
                        <div class="ib-icon">
                            <i class="flaticon-039-info"></i>
                        </div>
                        <div class="ib-text">
                            <h5>EVERY RE-TOP UP ID WILL HAVE 2 ENTRY,POOL ENTRY & GLOBAL LINE ENTRY AND AFTER 100 ID WILL CREDIT 50$ POOL INCOME AND 20$ GLOBAL LINE INCOME ON EVERY ENTRY.</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="icon-box-item">
                        <div class="ib-icon">
                            <i class="flaticon-039-info"></i>
                        </div>
                        <div class="ib-text">
                            <h5>ANY ID TO ID WALLET TRANSFER SYSTEM FOR TOP UP AND FUND PURCHASE AVAILABLE BY SYSTEM BTC ONLY.</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="icon-box-item">
                        <div class="ib-icon">
                            <i class="flaticon-039-info"></i>
                        </div>
                        <div class="ib-text">
                            <h5>MINIMUM 10 DIRECT ACTIVE ID WILL BE ROYALTY ACHIEVER OF TOTAL COMPANY TURN OVER 1% DIVIDED BY EQUAL IN ROYALTY ACHIEVERS MONTHLY BASIS.</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
