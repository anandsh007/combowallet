@extends('layouts.app')

@section('content')
<!-- Page top Section end -->
<section class="page-top-section set-bg" data-setbg="app/img/page-top-bg/4.jpg">
    <div class="container">
        <h2>Reach Us</h2>
        <nav class="site-breadcrumb">
            <a class="sb-item" href="#">Home</a>
            <span class="sb-item active">Reach Us</span>
        </nav>
    </div>
</section>
<!-- Page top Section end -->
<!-- Contact Section end -->
<section class="contact-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="contact-text">
                    <h2>Get in touch</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tem por incididunt ut labore et dolore mag na aliqua.  Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hime-naeos. Suspendisse potenti. </p>
                    <ul>
                        <li><i class="flaticon-032-placeholder"></i>291 Nara Thiwas Rajanakarin Road Sathorn Road,BANGKOK-
                            THAILAND-10120.</li>
                        <li><i class="flaticon-029-telephone-1"></i>+66.65874565</li>
                        <li><i class="flaticon-025-arroba"></i>globalcombowallet@gmail.com</li>
                        <li><i class="flaticon-038-wall-clock"></i>www.combowallet.info</li>
                    </ul>
                    <div class="social-links">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                        <a href="#"><i class="fa fa-pinterest"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-youtube-play"></i></a>
                    </div>
                </div>
            </div>
            {{-- <div class="col-lg-8">
                @include('includes.messages')
                <form class="contact-form" action="{{route('contact.submit')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" name="name" required placeholder="Your Name">
                        </div>
                        <div class="col-md-6">
                            <input type="email" name="email" required placeholder="Your E-mail">
                        </div>
                        <div class="col-md-12">
                            <input type="text" name="subject" required placeholder="Subject">
                            <textarea placeholder="Your Message" name="message" required></textarea>
                            <button class="site-btn">send message</button>
                        </div>
                    </div>
                </form>
            </div> --}}
        </div>
    </div>
</section>
<!-- Contact Section end -->
@endsection
