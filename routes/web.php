<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('reset-password/secret-que','Auth\ResetPasswordController@restPassQue');
Route::post('reset-password/secret-que','Auth\ResetPasswordController@secretQue')->name('password.secretQue');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index');
Route::get('/plan','HomeController@plan');
Route::get('/contact','HomeController@contact');
Route::get('/policy','HomeController@policy');
Route::get('/legal','HomeController@legal');
Route::post('/contact','HomeController@submit')->name('contact.submit');
Route::get('get-cities','Helper@getCities');
Route::get('get-sponsor-details','Helper@getSponsorDetails');
Route::get('account-top-up','User\UserController@showTopupForm')->name('account.topup');
Route::post('account-top-up','User\UserController@topup')->name('account.activate');
Route::post('file-upload','User\UserController@fileUpload')->name('fileUpload');
Route::get('fund-request','User\UserController@fundRequest')->name('fundRequest');
Route::get('user/logout', 'HomeController@logout')->name('user.logout');
/**
 * Helper Routes
 */
Route::get('validate/user-id','Helper@validateUserId')->name('helper.validateUserId');
Route::get('get/user-details/{userId}','Helper@getUserDetails')->name('helper.getUserDetails');

//******************* User Routes **********************//
Route::group(['prefix' => 'user','namespace' =>'User', 'middleware' => ['auth','account.status']], function () {
    Route::get('dashboard','UserController@index')->name('user.index');
    /**
     * Profile Routes
     */
    Route::get('view-profile','ProfileController@viewProfile')->name('profile.viewProfile');
    Route::get('sponsor-profile','ProfileController@sponsorProfile')->name('profile.sponsorProfile');
    Route::get('change-password','ProfileController@showChangePassForm')->name('profile.showChangePassForm');
    Route::post('change-password','ProfileController@changePassword')->name('profile.changePassword');
    Route::get('update-btc-address','ProfileController@showBtcForm')->name('profile.showBtcForm');
    Route::post('update-btc-address','ProfileController@updateBtcAddress')->name('profile.updateBtcAddress');
    Route::get('new-password','ProfileController@showNewPassForm')->name('profile.showNewPassForm');
    Route::post('new-password','ProfileController@newPassword')->name('profile.newPassword');
    Route::get('activation-history','ProfileController@activationHistory')->name('profile.activationHistory');

    /**
     * Team Routes
     */
    Route::get('direct-team','TeamController@directTeam')->name('team.directTeam');
    Route::get('registered-team','TeamController@registeredTeam')->name('team.registeredTeam');
    Route::get('total-team','TeamController@totalTeam')->name('team.totalTeam');
    Route::get('network','TeamController@network')->name('team.network');
    Route::get('network-chart','TeamController@networkChart')->name('team.networkChart');

    /**
     * Income Routes
     */
    Route::get('growth-income','IncomeController@growthIncome')->name('income.growthIncome');
    Route::get('direct-income','IncomeController@directIncome')->name('income.directIncome');
    Route::get('level-income','IncomeController@levelIncome')->name('income.levelIncome');
    Route::get('pool-income','IncomeController@poolIncome')->name('income.poolIncome');
    Route::post('transfer-fund','IncomeController@transferFund')->name('income.transferFund');
    Route::get('royalty-income','IncomeController@royaltyIncome')->name('income.royaltyIncome');
    Route::get('global-line-income','IncomeController@globalLineIncome')->name('income.globalLineIncome');

    /**
     * Wallet Routes
     */
    Route::get('transfer-fund','WalletController@showTransferForm')->name('wallet.transfer');
    Route::get('transfer-fund/transfer','WalletController@transferFund')->name('wallet.transferFund');
    Route::get('withdraw-fund','WalletController@showWithdrawForm')->name('wallet.showWithdrawForm');
    Route::post('withdraw-fund','WalletController@withdrawFund')->name('wallet.withdrawFund');
    Route::get('topup-id','WalletController@topupUserIdForm')->name('wallet.topupUserIdForm');
    Route::get('topup-id/topup','WalletController@topupId')->name('wallet.topupId');
    Route::get('fund-request-user','WalletController@userFundRequest')->name('wallet.userFundRequest');
    Route::get('withdrawal-request','WalletController@withdrawalRequest')->name('wallet.withdrawalRequest');
});

//******************* Admin Routes **********************//
Route::get('admin/login', 'admin\LoginController@loginForm')->name('admin.loginForm');
Route::post('admin-login', 'admin\LoginController@authenticate')->name('admin.authenticate');
Route::group(['prefix' => 'admin', 'middleware' => ['auth','role']], function () {
    Route::get('dashboard', 'admin\AdminController@index')->name('admin.dashboard');
    Route::get('add-city', 'admin\AdminController@addcityForm')->name('admin.addcityForm');
    Route::post('add-city', 'admin\AdminController@addCity')->name('admin.addCity');

    Route::get('logout', 'admin\LoginController@logout')->name('admin.logout');
    /** Profile Routes **/
    Route::resource('users', 'admin\UsersController');
    /** Profile Routes **/


    Route::get('change-admin-password', 'admin\UsersController@viewSecurity')->name('user.viewSecurity');
    Route::post('user-security', 'admin\UsersController@changeSecurity')->name('user.changeSecurity');

    /** Downline Routes **/
    Route::get('downline/network-tree','admin\DownlineController@networkTree')->name('downline.networkTree');
    Route::get('downline/total-downline','admin\DownlineController@totalDownline' )->name('downline.totalDownline');
    Route::get('downline/total-direct-team','admin\DownlineController@directTeam' )->name('downline.directTeam');
    Route::get('downline/total-blocked-list','admin\DownlineController@blockedMembers' )->name('downline.blockedMembers');
    /** Downline Routes **/

    /** Withdrawals Routes **/
    Route::get('withdrawals','admin\AdminController@withdrawalsList')->name('admin.withdrawalsList');
    Route::get('withdrawals/paid/{id}','admin\AdminController@withdrawalsPaid')->name('admin.withdrawalsPaid');
    Route::get('withdrawals/reject/{id}','admin\AdminController@withdrawalsRejected')->name('admin.withdrawalsRejected');
    /** Withdrawals Routes **/

    /** Fund Routes **/
    Route::get('add-fund','admin\FundController@addFundForm')->name('fund.addFundForm');
    Route::post('add-fund','admin\FundController@addFund')->name('fund.addFund');
    Route::get('added-user-fund','admin\FundController@fundList')->name('fund.fundList');
    Route::get('fund-request','admin\FundController@fundRequest')->name('fund.fundRequest');
    Route::get('fund-request-status-change','admin\FundController@changeFundRequestStatus')->name('fund.changeFundRequestStatus');
    /** Fund Routes **/

    /** Admin Actions Routes **/
    Route::get('actions','admin\ActionController@index')->name('action.index');
    Route::get('actions/status-change','admin\ActionController@adminAction')->name('action.adminAction');
    Route::resource('news', 'admin\\NewsController');
    /** Admin Actions Routes **/

    /* ******************** Export Data ******************* */
    Route::get('export/user-data','admin\UsersController@exportUserData')->name('exportUserData');
});
//******************************************************//
